Calculating Habitat Diversity
=======
Katie Gale, Fisheries and Oceans Canada (katie.gale@dfo-mpo.gc.ca)
27 March 2017; last update 5 March 2020

Calculating number of habitat types in 1 km nearshore planning units (grids).

## Input layers (detailed steps for prep in each feature's folder)

###### Kelp = "BCMCA_Kelp"
* BCMCA_ECO_GeneralKelp_Data
* BCMCA_ECO_BullKelp_Polygons_Data
* BCMCA_ECO_GiantKelp_Polygons_MARXAN
* BCMCA_ECO_BullKelp_Bioband_Data
* BCMCA_ECO_GiantKelp_Bioband_Data

###### Eelgrass = "BCMCA_Eelgrass"
* BCMCA_ECO_Eelgrass_Polygons_MARXAN
* BCMCA_ECO_Eelgrass_Bioband_Data

###### Surfgrass
* BCMCA_ECO_Surfgrass_Bioband_Data

###### Estuaries
* PECP_estuarypolys_with_ranking_March2007_updated2014

###### High Rugosity = "BCMCA_Rugosity"
* BCMCA_ECO_Physical_HighRugosity_Data

###### Geozones/Geomorphic Units (8 layers, Geozone_Crest, Geozone_CanyonFloor, etc)roj
* PMECS_Geomorphic_Units.gdb/geomorphic units

###### Bottom Patches (BoPs_BType1, 2, and 3)
* North Central Coast: NCC_BoPs_v1.1.gdb/BoP18_merged
* Haida Gwaii: HG_BoPs_v1.gdb/HG_BoPs
* Queen Charlotte Strait and Strait of Georgia: QCSSOG_BoPs_V1.0.gdb/BoP18_merged
* West Coast Vancouver Island: WCVI_BoPs_v1.gdb/BoPs
-----
## Steps
1. Convert biobands (polylines) to polygons
2. Merge polygons for kelp, eelgrass, and bottom patches to get one layer for each
3. Extract individual classes for geozones (8 layers: ridge, crest, depression, depression floor, mound, wall- sloping, wall- steeply sloping, canyon floor)
4. Extract individual classes for bottom patches (3 layers: 1 (hard), 2 (mixed), 3 (soft))
5. 16 layers total
6. Do spatial join of each layer to Nearshore_2km_PUs_V2.1. This gives a new PU shapefile with a column "count" for number of features (pieces of each layer).
7. Import the dbf files of all of the features into R. Convert counts into presence/absence, and sum the number of features present in each planning unit.
- Calculated total number of features (max 16), number of features excluding rugosity (15), number excluding geozones (8), number of biogenic-type habitats (kelp, eeglrass, surfgrass, and estuaries; max 4), and number of EBSA features (kelp, eelgrass, and estuaries; max 3).
- `Nearshore_2km_PUs_Habitats_v2.1.shp` has the biogenic column added
8. Export as shapefile to ArcMap.
9. Run hotspot analysis (Getis-Ord Gi*) with fixed distance band, Euclidean distance, and auto distance (1000.1 m). 
- Outputs are `Nearshore_2km_PUs_v2.1_Hotspots_v3.shp`

