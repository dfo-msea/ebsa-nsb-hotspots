#Maps of top deciles
library(sp)
library(rgdal)
library(maptools)
library(raster)
library(gridExtra)


###
setwd("E:/Documents/Projects/EBSAs/Analyses/Shelf/KernelDensity/")

# What folder to process?
#Survey1
folder1<-c("./diversityRichness_PHMA//10kmNeighbour_pooled/2.ReclassifyPercentile/")
survey1<-c("phma")
nbhd1<-c("10km")

#Survey2
folder2<-c("./diversityRichness_Synoptic//10kmNeighbour_pooled/2.ReclassifyPercentile/")
survey2<-c("synoptic")
nbhd2<-c("10km")

#Survey3
folder3<-c(NA)
survey3<-c("sablefish")
nbhd3<-c("19km")

#Bring in Important Areas
IA<-readOGR("G:/PMECSBackup/MANAGEMENT AREAS/Marine_Classifications/EBSAs/IAs NSB/ImportantAreas/PNCIMA/DFO_BC_IA_ROCFSH_LOCATE_PNCIMA_1.shp")

#Map output folder
mapFolder<-c("./TwoSurveys/")

# -------------------------------------------------#
# load polygons
shoreline <- readOGR("E:/Documents/!GIS Files/Coastlines/DFO_BC_COASTLIN_AND_BC_BDY_MERGE.shp")

nsb <- readOGR("E:/Documents/!GIS Files/Management_Classification_Areas/NSB.shp")
proj4string(nsb) <- CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")

contours <- readOGR("E:/Documents/!GIS Files/Bathymetry/bc_eez_100m/Derivatives/contours25m.shp")
proj4string(contours) <- CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")

# load rasters
phma.files<-data.frame(phma.files=list.files(folder1, ("*.tif$")))
phma.files$species<-gsub(paste0("kernel_",survey1,"_", nbhd1,"_"),"",phma.files$phma.files) 
phma.files$species<-gsub("_reclassify.tif","",phma.files$species)
phma.files$species<-c("fish richness","fish diversity")

syn.files<-data.frame(syn.files=list.files(folder2, ("*.tif$")))
syn.files$species<-gsub(paste0("kernel_",survey2,"_", nbhd2,"_"),"",syn.files$syn.files)
syn.files$species<-gsub("_reclassify.tif","",syn.files$species)
syn.files$species<-c("fish richness","fish diversity","invert richness","invert diversity")

sab.files<-data.frame(species=NA, sab.files=NA)
sab.files<-data.frame(sab.files=list.files(folder3, ("*.tif$")))
sab.files$species<-gsub(paste0("kernel_",survey3,"_", nbhd3,"_"),"",sab.files$sab.files)
sab.files$species<-gsub("_reclassify.tif","",sab.files$species)

files<-merge(phma.files, syn.files, by="species", all.x=T, all.y=T)
files<-merge(files, sab.files, by="species", all.x=T, all.y=T)

names<-read.delim("E:/Documents/Projects/MPAT/3.Conservation Priorities/3.Screen/CPList.txt", sep="\t")
names(names)[3]<-"species"
files<-merge(files, names, by="species", all.x=T, all.y=F)
files$CommonName<-gsub("\\s","\n",files$CommonName )


# phmaPt<-readOGR("E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/Shapefiles/PHMA/CPs/GFPHMA_Subsampled_TowxCtHook_TaxaInPU_CPs.shp")
# proj4string(phmaPt)<-CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")
# 
# synPt<-readOGR("E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/Shapefiles/Synoptics/CPs/GFSynopticSubsample_TotalWtPerHour_CPs.shp")
# proj4string(synPt)<-CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")
# 
# sabPt<-readOGR("E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/Shapefiles/Sablefish/GFsable_SetxCt_TaxaInPU_CPs.shp")
# proj4string(sabPt)<-CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")
# 

#Map parameters
ylim <- c(555000, 1100000)
xlim <- c(640000,760000  )

#axis labels
# y axis:
x <- -135 # x value in longitude at the y axis
ylab <- seq(-90,90,2) # you can adapt this sequence if you want more or less labels
yS <- SpatialPoints(cbind(x,ylab), proj4string = CRS("+proj=longlat +datum=NAD83"))
ySP<- spTransform(yS, proj4string(shoreline)) 

# x axis
y <- 50 # y value in latitude at the x axis
xlab = seq(-180,180,2)
xS <- SpatialPoints(cbind(xlab,y), proj4string = CRS("+proj=longlat +datum=NAD83"))
xSP<- spTransform(xS, proj4string(shoreline))

cola <- rgb(0,.6,.8, alpha=0.2)
colb <- rgb(0,.6,.8, alpha=0.2)
col1 <- rgb((244/255), (59/255), (32/255), alpha=0.7) #red
col2 <- rgb((217/255), (139/255), (23/255), alpha=0.7) #gold
col3 <- rgb((33/255), (161/255), (76/255), alpha=0.7) #green

##---###
# Loop plots -TOP 10% for PHMA and TOP 10% for SYNOPTIC and top 10% for SABLE overlaid
##---###
for (i in 1:nrow(files)){

if(is.na(files$phma.files[i])){
  phma <- raster()
} else {
  phma <- raster(paste0(folder1,files$phma.files[i]))
}

  if(is.na(files$syn.files[i])){
    syn <- raster()
  } else {
syn <- raster(paste0(folder2,files$syn.files[i], sep=""))
  }

  if(is.na(files$sab.files[i])){
    sab <- raster()
  } else {
    sab <- raster(paste0(folder3,files$sab.files[i], sep=""))
  }

#Pull out top 10% and bottom 90% of each raster
phmatop<-phma
phmabottom<-phma
if( all(is.na(values(phma)))){
  phmatop<-phmatop
  phmabottom<-phmabottom
} else {
phmatop[phmatop!=10]<-NA
phmabottom[!(phmabottom %in% c(1,2,3,4,5,6,7,8,9))]<-NA
}

syntop<-syn
synbottom<-syn
if( all(is.na(values(syn)))){
  syntop<-syntop
  synbottom<-synbottom
} else {
syntop[syntop!=10]<-NA
synbottom[!(synbottom %in% c(1,2,3,4,5,6,7,8,9))]<-NA
}

sabtop<-sab
sabbottom<-sabtop
if( all(is.na(values(sab)))){
  sabtop<-sabtop
  sabbottom<-sabbottom
} else {
  sabtop[sabtop!=10]<-NA
  sabbottom[!(sabbottom %in% c(1,2,3,4,5,6,7,8,9))]<-NA
}

#Make plot

png(paste0(mapFolder, "/combined_", files$species[i],".png", sep=""), height=15, width=12, units="cm", res=400)
par(mar=c(1,1,1,1))
plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
plot(contours[contours@data$CONTOUR==-500,], add=T)
plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7)
plot(contours[contours@data$CONTOUR==-50,], add=T, lty=2, lwd=0.7)

if (all(is.na(values(phmatop)))){
} else {
plot(phmatop, col=col3, add=T, legend=F)
}
if (all(is.na(values(syntop)))){
} else {
plot(syntop, col=col2, add=T, legend=F)
}
if (all(is.na(values(sabtop)))){
} else {
  plot(sabtop, col=col1, add=T, legend=F)
}

plot(nsb, add=T, col=NA, border="gray", lwd=0.8)
plot(shoreline, col="gray", border=NA, add=T)
# plot(IA, col=NA, border="blue", add=T, lwd=2)

rng <- par("usr") # Grab the plotting region dimensions
box( lty = 'solid', col = 'black')
axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
legend("bottomleft", legend=c(
  # "Species present", "Species absent",
  "PHMA", "Synoptic"
  # ,"500 m", "200 m", "50 m", "NSB"
  ),  title=c("Benthic Fish Diversity\nTop Decile of Kernel Density"), pch=c(
    # 1,16,
    15,15
    # ,NA,NA,NA,NA,NA
    ), lty=c(
      # 0,0,
      0,0
      # ,6,1,2,1,1
      ), lwd=c(
        # 0,0,
        0,0 
        # 1.2,0.7, 0.7, 0.8,2
        ),  col=c(
    # "red","blue",
    col3, col2
    # ,"black","black","black","gray", "blue"
    ), cex=0.7, bty="n"
  # , pt.cex=c(0.8, 0.2, 1,1
  )
  legend("bottom", legend=c("500 m", "200 m", "50 m", "NSB"),lty=c(6,1,2,1),lwd=c(1.2,0.7, 0.7, 0.8),col=c("black","black","black","gray"),cex=0.7, bty="n")



# legend("bottomleft", legend=c(
#   # "Species present", "Species absent",
#                               "PHMA - 10 km", "Synoptic - 10 km","SablefishTrap - 19 km","500 m", "200 m", "50 m", "NSB", "DFO Important Area"),
#        title=c("Top Decile of Kernel Density"), pch=c(
#          # 1,16,
#          15,15,15,NA,NA,NA,NA,NA), lty=c(
#            # 0,0,
#            0,0,0,6,1,2,1,1), lwd=c(
#              # 0,0,
#              0,0, 0, 1.2,0.7, 0.7, 0.8,2),
#        col=c(
#          # "red","blue",
#          col3, col2,col1,"black","black","black","gray", "blue"), cex=0.6
#   # , pt.cex=c(0.8, 0.2, 1,1)
#   )
# text(850000,ylim[2]-10000,  paste(files$CommonName[i]))
dev.off()
}



# ##---###
# # Loop plots - TOP 10%/BOTTOM 90% for PHMA and TOP 10%/BOTTOM 90% for SYNOPTIC as two separate plots
# ##---###
# for (i in 1:nrow(files)){
# 
# if(is.na(files$phma.files[i])){
#   phma<-raster()
# } else {
#   phma<-raster(paste0(folder1, files$phma.files[i]))
# }
# 
#   if(is.na(files$syn.files[i])){
#     syn<-raster()
#   } else {  
# syn<-raster(paste0(folder2,files$syn.files[i]))
# }
# 
# phmatop<-phma
# phmabottom<-phma
# 
# if( all(is.na(values(phma)))){
#   phmatop<-phmatop
#   phmabottom<-phmabottom
# } else {
# phmatop[phmatop!=10]<-NA
# phmabottom[!(phmabottom %in% c(1,2,3,4,5,6,7,8,9))]<-NA
# }
# 
# syntop<-syn
# synbottom<-syn
# if( all(is.na(values(syn)))){
#   syntop<-syntop
#   synbottom<-synbottom
# } else {
# syntop[syntop!=10]<-NA
# synbottom[!(synbottom %in% c(1,2,3,4,5,6,7,8,9))]<-NA
# }
# 
# #FIRST PLOT
# if (all(is.na(values(syn)))){
# } else {
# par(mar=c(0,0,0,0))
# png(paste0(mapFolder,"Hotspots/", files$species[i],"_", survey1, ".png"), height=15, width=12, units="cm", res=400)
# plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
# plot(contours[contours@data$CONTOUR==-500,], add=T)
# plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7)
# plot(contours[contours@data$CONTOUR==-50,], add=T, lty=2, lwd=0.7)

plot(synbottom, col=colb, add=T, legend=F)

plot(syntop, col=col2, add=T, legend=F)

plot(nsb, add=T, col=NA, border="gray", lwd=0.8)
plot(shoreline, col="gray", border=NA, add=T)

# b<-match(files[i,1],names(synPt))
# plot(synPt[synPt@data[,b]==0,], pch=16, cex=0.1, add=T, col="blue")
# plot(synPt[synPt@data[,b]>0,], pch=1, cex=.8, add=T, col="red")

rng <- par("usr") # Grab the plotting region dimensions
box( lty = 'solid', col = 'black')
axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
legend("bottomleft", legend=c(
  # "Species present", "Species absent", 
                              "Top decile", "Kernel density extent","500 m", "200 m", "50 m", "NSB"), 
       title=c("Synoptic"), pch=c(
         # 1,16,
         15,15,NA,NA,NA,NA), lty=c(
           # 0,0,
           0,0,6,1,2,1), lwd=c(
             # 0,0,
             0, 0, 1.2,0.7, 0.7, 0.8),
       col=c(
         # "red","blue",
         col2, cola,"black","black","black","gray"), cex=0.7
  # , pt.cex=c(0.8, 0.2, 1,1)
  )
text(850000,ylim[2]-10000,  paste(files$CommonName[i]))
dev.off()}

###SECOND PLOT
if (all(is.na(values(phma)))){
} else {
par(mar=c(0,0,0,0))
png(paste(paste0(mapFolder,"Hotspots/", files$species[i],"_", survey2, ".png")), height=15, width=12, units="cm", res=400)
plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
plot(contours[contours@data$CONTOUR==-500,], add=T,lty=6)
plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7)
plot(contours[contours@data$CONTOUR==-50,], add=T, lty=2, lwd=0.7)

plot(phmabottom, col=cola, add=T, legend=F)
plot(phmatop, col=col2, add=T, legend=F)

plot(nsb, add=T, col=NA, border="gray", lwd=0.8)
plot(shoreline, col="gray", border=NA, add=T)

# b<-match(files[i,1],names(phmaPt))
# plot(phmaPt[phmaPt@data[,b]==0,], pch=16, cex=0.1, add=T, col="blue")
# plot(phmaPt[phmaPt@data[,b]>0,], pch=1, cex=.8, add=T, col="red")

rng <- par("usr") # Grab the plotting region dimensions
box( lty = 'solid', col = 'black')
axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
legend("bottomleft", legend=c(
  # "Species present", "Species absent", 
  "Top decile", "Kernel density extent","500 m", "200 m", "50 m", "NSB"), 
       title=c("PHMA"), pch=c(
         # 1,16,
         15,15,NA,NA,NA,NA), lty=c(
           # 0,0,
           0,0,6,1,2,1), lwd=c(
             # 0,0,
                                   0, 0, 1.2,0.7, 0.7, 0.8),
       col=c(
         # "red","blue",
         col2, cola,"black","black","black","gray"), cex=0.7
  # , pt.cex=c(0.8, 0.2, 1,1)
  )
text(850000,ylim[2]-10000,  paste(files$CommonName[i]))
dev.off()}

}

# ##---###
# # Loop plots -ALL PERCENTILES (rainbow) for PHMA and SYNOPTIC as two separate plots
# ##---###
# for (i in 1:nrow(files)){
#   
#   colfunc <- colorRampPalette(c("royalblue","springgreen","yellow","red"))
#   colors<-list()
#   for (i in c(1:10)){
#     colors[i]<-paste(colfunc(10)[i], "CC",sep="")
#   
#   colors<-unlist(colors) }
#   
#   if(is.na(files$phma.files[i])){
#     phma<-raster()
#   } else {
#     phma<-raster(paste0(folder1, files$phma.files[i]))
#     phma[!(phma %in% c(1,2,3,4,5,6,7,8,9,10))]<-NA
#     }
#   
#   if(is.na(files$syn.files[i])){
#     syn<-raster()
#   } else {  
#     syn<-raster(paste0(folder2,files$syn.files[i]))
#     syn[!(syn %in% c(1,2,3,4,5,6,7,8,9,10))]<-NA
#     }
#   
#   #FIRST PLOT
#   if (all(is.na(values(syn)))){
#   } else {
#     par(mar=c(0,0,0,0))
#     png(paste0(mapFolder, "Percentiles/percentile_", files$species[i],survey2,".png"), height=15, width=12, units="cm", res=400)
#     plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
#     plot(contours[contours@data$CONTOUR==-500,], add=T)
#     plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7)
#     plot(contours[contours@data$CONTOUR==-50,], add=T, lty=2, lwd=0.7)
#     
#     plot(syn, add=T, legend=F, col=c(paste(colors)))
#     
#     plot(nsb, add=T, col=NA, border="gray", lwd=0.8)
#     plot(shoreline, col="gray", border=NA, add=T)
#     
#     b<-match(files[i,1],names(synPt))
#     plot(synPt[synPt@data[,b]==0,], pch=16, cex=0.2, add=T,col=rgb(0,0,1,alpha=0.5))
#     plot(synPt[synPt@data[,b]>0,], pch=1, cex=.3, add=T, col=rgb(0,0,0,alpha=0.6))
# 
#     rng <- par("usr") # Grab the plotting region dimensions
#     box( lty = 'solid', col = 'black')
#     axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
#     axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
#     legend("bottomleft", legend=c("1-10th","11-20th","21-30th","31-40th","41-50th","51-60th",
#                                   "61-70th","71-80th","81-90th","91-100th","Species present", "Species absent"),
#       title=c("Percentile"), pch=c(
#          15,15,15,15,15,15,15,15,15,15,1,16),    col=c(colfunc(10),"black","blue"), cex=0.7
#        , pt.cex=c( rep(1,10),0.4, 0.2))
#     legend("bottom", legend=c("500 m", "200 m", "50 m", "NSB"),lty=c(6,1,2,1),lwd=c(1.2,0.7, 0.7, 0.8),col=c("black","black","black","gray"),cex=0.7)
#     text(850000,ylim[2]-15000,  paste(files$CommonName[i], "\nSynoptic"))
#     dev.off()}
#   
#   ###SECOND PLOT
#   if (all(is.na(values(phma)))){
#   } else {
#     par(mar=c(0,0,0,0))
#     png(paste0(mapFolder, "Percentiles/percentile_", files$species[i],survey1,".png"), height=15, width=12, units="cm", res=400)
#     plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
#     plot(contours[contours@data$CONTOUR==-500,], add=T,lty=6)
#     plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7)
#     plot(contours[contours@data$CONTOUR==-50,], add=T, lty=2, lwd=0.7)
#     
#     plot(phma, add=T, legend=F, col=c(paste(colors)))
#     
#     plot(nsb, add=T, col=NA, border="gray", lwd=0.8)
#     plot(shoreline, col="gray", border=NA, add=T)
#     
#     b<-match(files[i,1],names(phmaPt))
#     plot(phmaPt[phmaPt@data[,b]==0,], pch=16, cex=0.2, add=T, col=rgb(0,0,1,alpha=0.5))
#     plot(phmaPt[phmaPt@data[,b]>0,], pch=1, cex=.3, add=T, col=rgb(0,0,0,alpha=0.6))
#     
#     rng <- par("usr") # Grab the plotting region dimensions
#     box( lty = 'solid', col = 'black')
#     axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
#     axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
#     legend("bottomleft", legend=c("1-10th","11-20th","21-30th","31-40th","41-50th","51-60th",
#                                   "61-70th","71-80th","81-90th","91-100th","Species present", "Species absent"),
#            title=c("Percentile"), pch=c(
#              15,15,15,15,15,15,15,15,15,15,1,16),    col=c(colfunc(10),"black","blue"), cex=0.7
#            , pt.cex=c( rep(1,10),0.4, 0.2))
#     legend("bottom", legend=c("500 m", "200 m", "50 m", "NSB"),lty=c(6,1,2,1),lwd=c(1.2,0.7, 0.7, 0.8),col=c("black","black","black","gray"),cex=0.7)
#     text(850000,ylim[2]-15000,  paste(files$CommonName[i], "\nPHMA"))
#     dev.off()}
# 
# }


# ######---------------############
# #Bring in individual rasters for making maps
# #Ugly code, need to fix
# ######---------------############
# 
phmaFish<-raster("./totalBiomass_PHMA/10kmNeighbour_Pooled/2.ReclassifyPercentile/kernel__10km_GFPHMA_totalBiomassFish_reclassify.tif")
synFish<-raster("./totalBiomass_Synoptic//10kmNeighbour_Pooled/2.ReclassifyPercentile/kernel__10km_GFSynoptic_totalBiomassFish_reclassify.tif")
synInv<-raster("./totalBiomass_Synoptic/10kmNeighbour_Pooled/2.ReclassifyPercentile/kernel__10km_GFSynoptic_totalBiomassInvert_reclassify.tif")
# halibut10km<-raster("./Halibut_Synoptic//10kmNeighbour_pooled/2.ReclassifyPercentile//kernel_GFSynopticSubsample_HalibutAlb_reclassify.tif")
#   
phmaFishBottom<-phmaFish
synFishBottom<-synFish
synInvBottom<-synInv
# halibut10kmBottom<-halibut10km
# 
phmaFishBottom[!(phmaFishBottom %in% c(1,2,3,4,5,6,7,8,9))]<-NA
synFishBottom[!(synFishBottom %in% c(1,2,3,4,5,6,7,8,9))]<-NA
synInvBottom[!(synInvBottom %in% c(1,2,3,4,5,6,7,8,9))]<-NA
# halibut10kmBottom[!(halibut10kmBottom %in% c(1,2,3,4,5,6,7,8,9))]<-NA
# 
phmaFishTop<-phmaFish
synFishTop<-synFish
synInvTop<-synInv
# halibut10kmTop<-halibut10km
# 
phmaFishTop[phmaFishTop!=10]<-NA
synFishTop[synFishTop!=10]<-NA
synInvTop[synInvTop!=10]<-NA
# halibut10kmTop[halibut10kmTop!=10]<-NA
# 
phmaFish[!(phmaFish %in% c(1,2,3,4,5,6,7,8,9,10))]<-NA
synFish[!(synFish %in% c(1,2,3,4,5,6,7,8,9,10))]<-NA
synInv[!(synInv %in% c(1,2,3,4,5,6,7,8,9,10))]<-NA
# halibut10km[!(halibut10km %in% c(1,2,3,4,5,6,7,8,9,10))]<-NA

# # ##---###
# # # BIOMASS plots -- TOP 10% for PHMA FISH, TOP 10% for SYNOPTIC FISH, and TOP 10% for SYNOPTIC INVERTS, overlaid
# # ##---###
{
  png(paste("./TwoSurveys/fish_biomass_combined.png", sep=""), height=15, width=12, units="cm", res=400)
  par(mar=c(1,1,1,1))
  plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
  plot(contours[contours@data$CONTOUR==-500,], add=T)
  plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7)
  plot(contours[contours@data$CONTOUR==-50,], add=T, lty=2, lwd=0.7)

    plot(phmaFishTop, col=col3, add=T, legend=F)
    plot(synFishTop, col=col2, add=T, legend=F)
    # plot(synInvTop, col=col1, add=T, legend=F)

  plot(nsb, add=T, col=NA, border="gray", lwd=0.8)
  plot(shoreline, col="gray", border=NA, add=T)

  rng <- par("usr") # Grab the plotting region dimensions
  box( lty = 'solid', col = 'black')
  axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
  axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
  legend("bottomleft", legend=c("PHMA - 10 km", "Synoptic - 10 km","500 m", "200 m", "50 m", "NSB"),
    title=c("Top Decile of Kernel Density"), pch=c(15, 15,NA,NA,NA,NA), lty=c(0,0,1,1,2,1), lwd=c(0,0,  1.2,0.7, 0.7, 0.8), col=c(col3, col2,"black","black","black","gray"), cex=0.7,bty="n")
  # legend("bottomleft", legend=c("Top decile - PHMA Fish", "Top decile - Synoptic Fish","Top decile - Synoptic Inverts","500 m", "200 m", "50 m", "NSB"),
  #        title=c("Kernel Density"), pch=c(15, 15,15,NA,NA,NA,NA), lty=c(0,0,0,6,1,2,1), lwd=c(0,0, 0, 1.2,0.7, 0.7, 0.8), col=c(col3, col2,col1,"black","black","black","gray"), cex=0.7)
  # 
  text(850000,ylim[2]-10000,  "Fish\nBiomass")
  dev.off()}

# ##---###
# # BIOMASS plots -- PERCENTILES (rainbow) for PHMA FISH, SYNOPTIC FISH, and SYNOPTIC INVERTS as three separate plots
# ##---###
{colfunc <- colorRampPalette(c("royalblue","springgreen","yellow","red"))
colors<-list()
for (i in c(1:10)){
colors[i]<-paste(colfunc(10)[i], "CC",sep="")}
colors<-unlist(colors)
#
#synoptic fish
  {
    png(paste("./totalBiomass_Synoptic/percentile_biomass_synFish.png", sep=""), height=15, width=12, units="cm", res=400)
    par(mar=c(1,1,1,1))
    plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
    plot(contours[contours@data$CONTOUR==-500,], add=T)
    plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7)
    plot(contours[contours@data$CONTOUR==-50,], add=T, lty=2, lwd=0.7)
    plot(synFish, add=T, legend=F, col=c(paste(colors)))
    plot(nsb, add=T, col=NA, border="gray", lwd=0.8)
    plot(shoreline, col="gray", border=NA, add=T)
    plot(synPt, pch=16, cex=0.2, add=T,col=rgb(0,0,1,alpha=0.5))
    rng <- par("usr") # Grab the plotting region dimensions
    box( lty = 'solid', col = 'black')
    axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
    axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
    legend("bottomleft", legend=c("1-10th","11-20th","21-30th","31-40th","41-50th","51-60th","61-70th","71-80th","81-90th","91-100th","Sampling location"),
      title=c("Percentile"), pch=c(15,15,15,15,15,15,15,15,15,15,1,16),    col=c(colfunc(10),"blue"), cex=0.7       , pt.cex=c( rep(1,10),0.4, 0.2),bty="n")
    legend("bottom", legend=c("500 m", "200 m", "50 m", "NSB"),lty=c(1,1,2,1),lwd=c(1.2,0.7, 0.7, 0.8),col=c("black","black","black","gray"),cex=0.7,bty="n")
    text(850000,ylim[2]-10000,  paste(c("Synoptic Fish\nBiomass")))
    dev.off()}

#synoptic invert
    {
    png(paste("./totalBiomass_Synoptic/percentile_biomass_synInvert.png", sep=""), height=15, width=12, units="cm", res=400)
    par(mar=c(1,1,1,1))
    plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
    plot(contours[contours@data$CONTOUR==-500,], add=T)
    plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7)
    plot(contours[contours@data$CONTOUR==-50,], add=T, lty=2, lwd=0.7)
    plot(synInv, add=T, legend=F, col=c(paste(colors)))
    plot(nsb, add=T, col=NA, border="gray", lwd=0.8)
    plot(shoreline, col="gray", border=NA, add=T)
    plot(synPt, pch=16, cex=0.2, add=T,col=rgb(0,0,1,alpha=0.5))
    rng <- par("usr") # Grab the plotting region dimensions
    box( lty = 'solid', col = 'black')
    axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
    axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
    legend("bottomleft", legend=c("1-10th","11-20th","21-30th","31-40th","41-50th","51-60th","61-70th","71-80th","81-90th","91-100th","Sampling location"),
           title=c("Percentile"), pch=c(15,15,15,15,15,15,15,15,15,15,1,16),    col=c(colfunc(10),"blue"), cex=0.7, pt.cex=c( rep(1,10),0.4, 0.2),bty="n")
    legend("bottom", legend=c("500 m", "200 m", "50 m", "NSB"),lty=c(1,1,2,1),lwd=c(1.2,0.7, 0.7, 0.8),col=c("black","black","black","gray"),cex=0.7,bty="n")
    text(850000,ylim[2]-10000,  paste(c("Synoptic Invert\nBiomass")))
    dev.off()}
#
#PHMA fish
    {
    png(paste("./totalBiomass_PHMA/percentile_biomass_phmaFish.png", sep=""), height=15, width=12, units="cm", res=400)
    par(mar=c(1,1,1,1))
    plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
    plot(contours[contours@data$CONTOUR==-500,], add=T)
    plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7)
    plot(contours[contours@data$CONTOUR==-50,], add=T, lty=2, lwd=0.7)
    plot(phmaFish, add=T, legend=F, col=c(paste(colors)))
    plot(nsb, add=T, col=NA, border="gray", lwd=0.8)
    plot(shoreline, col="gray", border=NA, add=T)
    plot(phmaPt, pch=16, cex=0.2, add=T,col=rgb(0,0,1,alpha=0.5))
    rng <- par("usr") # Grab the plotting region dimensions
    box( lty = 'solid', col = 'black')
    axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
    axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
    legend("bottomleft", legend=c("1-10th","11-20th","21-30th","31-40th","41-50th","51-60th","61-70th","71-80th","81-90th","91-100th","Sampling location"),
           title=c("Percentile"), pch=c(15,15,15,15,15,15,15,15,15,15,1,16),    col=c(colfunc(10),"blue"), cex=0.7, pt.cex=c( rep(1,10),0.4, 0.2),bty="n")
    legend("bottom", legend=c("500 m", "200 m", "50 m", "NSB"),lty=c(1,1,2,1),lwd=c(1.2,0.7, 0.7, 0.8),col=c("black","black","black","gray"),cex=0.7,bty="n")
    text(850000,ylim[2]-10000,  paste(c("PHMA Fish\nBiomass")))
    dev.off()}
}
#
#
##---###
# BIOMASS plots -- TOP 10%/BOTTOM 90% for PHMA FISH, SYNOPTIC FISH, and SYNOPTIC INVERTS as three separate plots
##---###
{
## PHMA FISH
{png(paste("./totalBiomass_PHMA/topPercentile_biomass_phmaFish.png", sep=""), height=15, width=12, units="cm", res=400)
  par(mar=c(1,1,1,1))
plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
plot(contours[contours@data$CONTOUR==-500,], add=T)
plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7)
plot(contours[contours@data$CONTOUR==-50,], add=T, lty=2, lwd=0.7)
plot(phmaFishBottom, col=colb, add=T, legend=F)
plot(phmaFishTop, col=col2, add=T, legend=F)
plot(nsb, add=T, col=NA, border="gray", lwd=0.8)
plot(shoreline, col="gray", border=NA, add=T)
rng <- par("usr") # Grab the plotting region dimensions
box( lty = 'solid', col = 'black')
axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
legend("bottomleft", legend=c("Top decile", "Kernel density extent","500 m", "200 m", "50 m", "NSB"),pch=c(15,15,NA,NA,NA,NA), lty=c(0,0,1,1,2,1), lwd=c(0, 0, 1.2,0.7, 0.7, 0.8),col=c(col2, cola,"black","black","black","gray"), cex=0.7,bty="n")
text(850000,ylim[2]-10000,  "PHMA Fish\nBiomass")
dev.off()}

##Synoptic fish
{png(paste("./totalBiomass_Synoptic/topPercentile_biomass_synFish.png", sep=""), height=15, width=12, units="cm", res=400)
  par(mar=c(1,1,1,1))
plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
plot(contours[contours@data$CONTOUR==-500,], add=T)
plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7)
plot(contours[contours@data$CONTOUR==-50,], add=T, lty=2, lwd=0.7)
plot(synFishBottom, col=colb, add=T, legend=F)
plot(synFishTop, col=col2, add=T, legend=F)
plot(nsb, add=T, col=NA, border="gray", lwd=0.8)
plot(shoreline, col="gray", border=NA, add=T)
rng <- par("usr") # Grab the plotting region dimensions
box( lty = 'solid', col = 'black')
axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
legend("bottomleft", legend=c("Top decile", "Kernel density extent","500 m", "200 m", "50 m", "NSB"),
       pch=c(15,15,NA,NA,NA,NA), lty=c(0,0,1,1,2,1), lwd=c(0, 0, 1.2,0.7, 0.7, 0.8),col=c(col2, cola,"black","black","black","gray"), cex=0.7,bty="n")
text(850000,ylim[2]-10000,  "Synoptic Fish\nBiomass")
dev.off()}
#
## Synoptic Inverts
{png(paste("./totalBiomass_Synoptic/topPercentile_biomass_synInv.png", sep=""), height=15, width=12, units="cm", res=400)
  par(mar=c(1,1,1,1))
plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
plot(contours[contours@data$CONTOUR==-500,], add=T)
plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7)
plot(contours[contours@data$CONTOUR==-50,], add=T, lty=2, lwd=0.7)
plot(synInvBottom, col=colb, add=T, legend=F)
plot(synInvTop, col=col2, add=T, legend=F)
plot(nsb, add=T, col=NA, border="gray", lwd=0.8)
plot(shoreline, col="gray", border=NA, add=T)
box( lty = 'solid', col = 'black')
axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
legend("bottomleft", legend=c("Top decile", "Kernel density extent","500 m", "200 m", "50 m", "NSB"),pch=c(15,15,NA,NA,NA,NA), lty=c(0,0,1,1,2,1), lwd=c(0, 0, 1.2,0.7, 0.7, 0.8),col=c(col2, cola,"black","black","black","gray"), cex=0.7,bty="n")
text(850000,ylim[2]-10000,  "Synoptic Invert\nBiomass")
dev.off()}
  }
# 
# ##---###
# # HALIBUT 10km plot -- PERCENTILES (rainbow)
# ##---###
# {par(mar=c(0,0,0,0))
#  png(paste("./Halibut_Synoptic//10kmNeighbour_pooled/4.Maps/percentile_Halibut10km.png", sep=""), height=15, width=12, units="cm", res=400)
#  plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
#  plot(contours[contours@data$CONTOUR==-500,], add=T)
#  plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7)
#  plot(contours[contours@data$CONTOUR==-50,], add=T, lty=2, lwd=0.7)
#  plot(halibut10km, add=T, legend=F, col=c(paste(colors)))
#  plot(nsb, add=T, col=NA, border="gray", lwd=0.8)
#  plot(shoreline, col="gray", border=NA, add=T)
#  rng <- par("usr") # Grab the plotting region dimensions
#  box( lty = 'solid', col = 'black')
#  axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
#  axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
#  legend("bottomleft", legend=c("1-10th","11-20th","21-30th","31-40th","41-50th","51-60th","61-70th","71-80th","81-90th","91-100th"),
#         title=c("Percentile"), pch=c(15,15,15,15,15,15,15,15,15,15,1),    col=c(colfunc(10)), cex=0.7, pt.cex=c( rep(1,10),0.4))
#  legend("bottom", legend=c("500 m", "200 m", "50 m", "NSB"),lty=c(6,1,2,1),lwd=c(1.2,0.7, 0.7, 0.8),col=c("black","black","black","gray"),cex=0.7)
#  text(850000,ylim[2]-15000,  paste(c("Halibut 10km\nneighbour")))
#  dev.off()}
# 
# ##---###
# # HALIBUT 10km plot -- TOP 10%/BOTTOM 90%
# ##---###
# {png(paste("./Halibut_Synoptic//10kmNeighbour_pooled/4.Maps/topPercentile_Halibut10km.png", sep=""), height=15, width=12, units="cm", res=400)
#  plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
#  plot(contours[contours@data$CONTOUR==-500,], add=T)
#  plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7)
#  plot(contours[contours@data$CONTOUR==-50,], add=T, lty=2, lwd=0.7)
#  plot(halibut10kmBottom, col=colb, add=T, legend=F)
#  plot(halibut10kmTop, col=col2, add=T, legend=F)
#  plot(nsb, add=T, col=NA, border="gray", lwd=0.8)
#  plot(shoreline, col="gray", border=NA, add=T)
#  rng <- par("usr") # Grab the plotting region dimensions
#  box( lty = 'solid', col = 'black')
#  axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
#  axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
#  legend("bottomleft", legend=c("Top decile", "Kernel density extent","500 m", "200 m", "50 m", "NSB"),pch=c(15,15,NA,NA,NA,NA), lty=c(0,0,6,1,2,1), lwd=c(0, 0, 1.2,0.7, 0.7, 0.8),col=c(col2, cola,"black","black","black","gray"), cex=0.7)
#  text(850000,ylim[2]-10000,  "Halibut 10km\nneighbour")
#  dev.off()}