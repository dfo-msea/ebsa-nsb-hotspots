#Make standard maps for fish kernel density analyses
#Katie Gale, 12 Dec 2017

#Maps of top deciles
library(sp)
library(rgdal)
library(maptools)
library(raster)
# library(gridExtra)

setwd("E:/Documents/Data files/MPATT Data/Groundfish/")

####
analysis<-"Synoptic_PU"
nbhd<-"10km"

inFolder<-c(paste0("./",analysis,"/2.ReclassifyPercentile/"))
mapFolder<-c(paste0("./",analysis,"/Maps/"))

dataPoints<-readOGR(paste0("./",analysis, "/Input/SynopticFELinesSp.shp"))

# -------------------------------------------------#
# load rasters and set up lookup table for species names
KDfiles<-list.files(inFolder, ("*.tif$"))
KDspecies<-gsub(paste0("kernel_",analysis,"_", nbhd,"_"),"",KDfiles)
KDspecies<-as.data.frame(gsub("_reclassify.tif","",KDspecies))
KDspecies$file<-KDfiles
names(KDspecies)<-c("species", "file")
names<-read.delim("E:/Documents/Projects/MPAT/3.Conservation Priorities/3.Screen/CPList2.txt", sep="\t")
names(names)[3]<-"species"
names<-merge(KDspecies, names, by="species", all.x=F, all.y=F)
names$CommonName<-gsub("\\s","\n",names$CommonName )

# # # -------------------------------------------------#
# # # load polygons
shoreline <- readOGR("E:/Documents/!GIS Files/Coastlines/DFO_BC_COASTLIN_AND_BC_BDY_MERGE.shp")

nsb <- readOGR("E:/Documents/!GIS Files/Management_Classification_Areas/NSB.shp")
proj4string(nsb) <- CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")

contours <- readOGR("E:/Documents/!GIS Files/Bathymetry/bc_eez_100m/Derivatives/contours25m.shp")
proj4string(contours) <- CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")

# -------------------------------------------------#
# #Map parameters
# extent for phma/synoptic footprint
ylim <- c(555000, 1100000)
xlim <- c(640000,760000  )

# extent for full NSB
# ylim <- c(490000, 1105000)
# xlim <- c(470000,1090000  )

#axis labels
# y axis:
x <- -135 # x value in longitude at the y axis
ylab <- seq(-90,90,2) # you can adapt this sequence if you want more or less labels
yS <- SpatialPoints(cbind(x,ylab), proj4string = CRS("+proj=longlat +datum=NAD83"))
ySP<- spTransform(yS, proj4string(shoreline)) 

# x axis
y <- 50 # y value in latitude at the x axis
xlab = seq(-180,180,2)
xS <- SpatialPoints(cbind(xlab,y), proj4string = CRS("+proj=longlat +datum=NAD83"))
xSP<- spTransform(xS, proj4string(shoreline))

#Color options
cola <- rgb(0,.6,.8, alpha=0.2)
colb <- rgb(0,.6,.8, alpha=0.2)
col1 <- rgb((244/255), (59/255), (32/255), alpha=0.7) #red
col2 <- rgb((217/255), (139/255), (23/255), alpha=0.7) #gold
col3 <- rgb((33/255), (161/255), (76/255), alpha=0.7) #green

##---###
# Loop plots - TOP 10%/BOTTOM 90% for Given Folder
##---###

for (i in 1:nrow(names)){

  ras<-raster(paste0(folder1, names[i,2]))
  rastop<-ras
  rasbottom<-ras
  
  rastop[rastop!=10]<-NA
  rasbottom[!(rasbottom %in% c(1,2,3,4,5,6,7,8,9))]<-NA
  ras[!(ras %in% c(1,2,3,4,5,6,7,8,9,10))]<-NA

    png(paste0(mapFolder,"Hotspots/hotspot_", names$species[i],"_", survey1, ".png"), height=15, width=12, units="cm", res=400)
    par(mar=c(1,1,1,1))
    plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
    plot(contours[contours@data$CONTOUR==-500,], add=T)
    plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7)
    plot(contours[contours@data$CONTOUR==-50,], add=T, lty=2, lwd=0.7)
    plot(rasbottom, col=colb, add=T, legend=F)
    plot(rastop, col=col2, add=T, legend=F)
    plot(nsb, add=T, col=NA, border="gray", lwd=0.8)
    plot(shoreline, col="gray", border=NA, add=T)
    rng <- par("usr") # Grab the plotting region dimensions
    box( lty = 'solid', col = 'black')
    axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
    axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
    legend("bottomleft", legend=c("Top decile", "Kernel density extent","500 m", "200 m", "50 m", "NSB"),
           title=paste0(surveyName, ", ",nbhd, " km"), pch=c(15,15,NA,NA,NA,NA), lty=c(0,0,6,1,2,1), lwd=c(0, 0, 1.2,0.7, 0.7, 0.8),col=c(col2, cola,"black","black","black","gray"), cex=0.7)
    text(850000,ylim[2]-10000,  paste(names$CommonName[i]))
    dev.off()
}

##---###
# Loop plots -- PERCENTILES (rainbow)
##---###
{colfunc <- colorRampPalette(c("royalblue","springgreen","yellow","red"))
 colors<-list()
 for (i in c(1:10)){
   colors[i]<-paste(colfunc(10)[i], "CC",sep="")}
 colors<-unlist(colors)

 for (i in 1:nrow(names)){
   for (i in c(6,63:67)){   
   ras<-raster(paste0(folder1, names[i,2]))
   rastop<-ras
   rasbottom<-ras
   rastop[rastop!=10]<-NA
   rasbottom[!(rasbottom %in% c(1,2,3,4,5,6,7,8,9))]<-NA
   ras[!(ras %in% c(1,2,3,4,5,6,7,8,9,10))]<-NA

png(paste0(mapFolder,"Percentiles/percentile_", names$species[i],"_", survey1, ".png"), height=15, width=12, units="cm", res=400)
 par(mar=c(1,1,1,1))
 plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
 plot(contours[contours@data$CONTOUR==-500,], add=T, lwd=0.8,col="black")
 plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7, col="black")
 plot(contours[contours@data$CONTOUR==-50,], add=T, lwd=0.4, col="black")
 
 plot(nsb, add=T, col=NA, lwd=0.7, lty=5, lwd=1.5)
 plot(shoreline, col="gray", border="gray", add=T)
plot(ras, add=T, legend=F, col=c(paste(colors)))
#  plot(synPt, pch=16, cex=0.2, add=T,col=rgb(0,0,1,alpha=0.5))
 rng <- par("usr") # Grab the plotting region dimensions
 box( lty = 'solid', col = 'black')
 axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
 axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
#  legend("bottomleft", legend=c("1-10th","11-20th","21-30th","31-40th","41-50th","51-60th","61-70th","71-80th","81-90th","91-100th","Sampling location"),
#         title=c("Percentile"), pch=c(15,15,15,15,15,15,15,15,15,15,1,16),    col=c(colfunc(10),"blue"), cex=0.7       , pt.cex=c( rep(1,10),0.4, 0.2))
legend("bottomleft", legend=c("1-10th","11-20th","21-30th","31-40th","41-50th","51-60th","61-70th","71-80th","81-90th","91-100th"),
       title=c("Percentile"), pch=c(15,15,15,15,15,15,15,15,15,15),    col=c(colfunc(10)), cex=0.7       , pt.cex=c( rep(1,10),0.4))
legend("bottom", legend=c("500 m", "200 m", "50 m", "NSB"),lty=c(1,1,1,5),lwd=c(.8,0.7, 0.4, 1.5),col=c("black","black","black","black"),cex=0.7,
#         title=paste0(surveyName, ", ",nbhd, " km"))
          title="Synoptic Trawl Survey")
text(900000,ylim[2]+56000,   paste(names$CommonName[i], "\nSynoptic Trawl Survey"))
 dev.off()}
}

##---###
# Loop plots -- PERCENTILES (rainbow) with sample points
##---###
{colfunc <- colorRampPalette(c("royalblue","springgreen","yellow","red"))
 colors<-list()
 for (i in c(1:10)){
   colors[i]<-paste(colfunc(10)[i], "CC",sep="")}
 colors<-unlist(colors)
 
 for (i in 1:nrow(names)){
 for (i in c(14)){
   ras<-raster(paste0(folder1, names[i,2]))
   rastop<-ras
   rasbottom<-ras
   rastop[rastop!=10]<-NA
   rasbottom[!(rasbottom %in% c(1,2,3,4,5,6,7,8,9))]<-NA  
   ras[!(ras %in% c(1,2,3,4,5,6,7,8,9,10))]<-NA
   
   spPt<-dataPoints[,names(dataPoints)==names[i,1]]
   names(spPt)[1]<-"species"
   
   png(paste0(mapFolder,"Percentiles/point_percentile_", names$species[i],"_", survey1, "3.png"), height=15, width=12, units="cm", res=400)
   par(mar=c(1,1,1,1))
   plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
   plot(contours[contours@data$CONTOUR==-500,], add=T, col="black")
   plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7, col="black")
   plot(contours[contours@data$CONTOUR==-50,], add=T, lty=2, lwd=0.7, col="black")
   plot(ras, add=T, legend=F, col=c(paste(colors)))
   plot(nsb, add=T, col=NA, border="gray", lwd=0.8)
   plot(shoreline, col="gray", border=NA, add=T)
   plot(spPt[spPt$species>0,], pch=21, cex=0.6, add=T,bg=rgb(0,0,0,alpha=0.2), col="black")
   plot(spPt[spPt$species==0,], pch=4, cex=0.2, add=T,col=rgb(0,0,1,alpha=0.5))
   rng <- par("usr") # Grab the plotting region dimensions
   box( lty = 'solid', col = 'black')
   axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
   axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
   #  legend("bottomleft", legend=c("1-10th","11-20th","21-30th","31-40th","41-50th","51-60th","61-70th","71-80th","81-90th","91-100th","Sampling location"),
   #         title=c("Percentile"), pch=c(15,15,15,15,15,15,15,15,15,15,1,16),    col=c(colfunc(10),"blue"), cex=0.7       , pt.cex=c( rep(1,10),0.4, 0.2))
   legend("bottomleft", legend=c("1-10th","11-20th","21-30th","31-40th","41-50th","51-60th","61-70th","71-80th","81-90th","91-100th", "Species present","Species absent"),
          title=c("Percentile"), pch=c(15,15,15,15,15,15,15,15,15,15, 21,4),    col=c(colfunc(10), "black","blue"), cex=0.7       , pt.cex=c( rep(1,10),0.6,0.2))
   legend("bottom", legend=c("500 m", "200 m", "50 m", "NSB"),lty=c(6,1,2,1),lwd=c(1.2,0.7, 0.7, 0.8),col=c("black","black","black","gray"),cex=0.7,
          title=paste0(surveyName, ", ",nbhd, " km"))
   text(850000,ylim[2]-15000,   paste(names$CommonName[i]))
   dev.off()
 }}
