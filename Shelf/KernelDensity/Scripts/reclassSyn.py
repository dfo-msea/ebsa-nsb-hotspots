# Name: reclassSyn.py
# Description: Loop through kernel density rasters, calculate decile, and output reclassed raster with values of 1-10.
# Author: Katie Gale
# Date: 2 August 2017


# Import system modules
import numpy
import arcpy
from arcpy import env
from arcpy.sa import *

# Set environment settings
arcpy.env.workspace = "D:/Documents/Data files/MPATT Data/DataLayers/Groundfish/RougheyeAll_2018.10.03/1.KernelDensity"
inwk=env.workspace+"/"
outwk=inwk.replace("1.KernelDensity","")+"/2.ReclassifyPercentile/"
years= arcpy.ListRasters ()


for i in xrange(0,len(years)):
    # Set local variables
    inFeatures = inwk+years[i]

#arr=arrAll[numpy.nonzero(arrAll)]
    arrAll=arcpy.RasterToNumPyArray (inFeatures)
    arrAll[arrAll <= 0] = numpy.nan
    arr=arrAll
    p10=numpy.nanpercentile(arr,10)
    p20=numpy.nanpercentile(arr,20)
    p30=numpy.nanpercentile(arr,30)
    p40=numpy.nanpercentile(arr,40)
    p50=numpy.nanpercentile(arr,50)
    p60=numpy.nanpercentile(arr,60)
    p70=numpy.nanpercentile(arr,70)
    p80=numpy.nanpercentile(arr,80)
    p90=numpy.nanpercentile(arr,90)
    p100=numpy.nanpercentile(arr,100)

    minArr=numpy.nanmin(arr)
    remapTable=RemapRange([[0,minArr, "NODATA"],[minArr,p10,1],[p10,p20,2],[p20,p30,3],[p30,p40,4],[p40,p50,5],[p50,p60,6],[p60,p70,7],[p70,p80,8],[p80,p90,9],[p90,p100,10]])
    tab2=[[0,minArr, "NODATA"],[minArr,p10,1],[p10,p20,2],[p20,p30,3],[p30,p40,4],[p40,p50,5],[p50,p60,6],[p60,p70,7],[p70,p80,8],[p80,p90,9],[p90,p100,10]]
#remapTable=RemapRange([[0,p10,1],[p10,p20,2],[p20,p30,3],[p30,p40,4],[p40,p50,5],[p50,p60,6],[p60,p70,7],[p70,p80,8],[p80,p90,9],[p90,p100,10]])

    outReclassRR = Reclassify(inFeatures, "VALUE", remapTable)

    name=years[i]
    outname=name.replace(".tif","_reclassify.tif")
    outfile=outwk+outname
    outReclassRR.save(outfile)
