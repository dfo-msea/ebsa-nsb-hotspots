# Name: runKernelLoopSpecies.py
# Description: For a site x species shapefile, loop through columns (species) and produce kernel density rasters based on a given field and neighbourhood size
# Requirements: Spatial Analyst Extension
# Author: Katie Gale
# Date: 2 August 2017

# Import system modules
import arcpy
from arcpy import env
from arcpy.sa import *

dataLabel="SynopticGFTrawlRescaleAllBC"

# Set environment settings
arcpy.env.workspace = "D:/Documents/Data files/MPATT Data/DataLayers/Groundfish/RougheyeAll_2018.10.03/Input"
#arcpy.env.workspace = "D:/Documents/Data files/SpongeCoral/updateImportantAreas/forSuzukiSep2018/Input"

inwk=arcpy.env.workspace+"/"
outwk=inwk.replace("Input","")+"1.KernelDensity/"

#arcpy.env.extent = arcpy.Extent(459060, 1227650, 1087060, 539650) #PUs  #left,top,right,bottom
arcpy.env.extent = arcpy.Extent(417800, 336000, 1142400, 1237800) #NSB
#arcpy.env.extent = arcpy.Extent(417800, 316000, 1142400, 1237800) #All of BC
#arcpy.env.extent = arcpy.Extent(459060, 1227650, 1142400, 329650) #All of BC to match MPATT grid

shape= arcpy.ListFeatureClasses ()

def getFieldNames(shp):
        fieldnames = [f.name for f in arcpy.ListFields(shp)]
        return fieldnames
fieldnames = getFieldNames(inwk+shape[0])

#for i in xrange(7,(len(fieldnames)-6)):
for i in (3,4): 
#for i in xrange(51,52): 
#for i in xrange(77,79):
    # Set local variables
    inFeatures = inwk+shape[0]
    populationField = fieldnames[i]
    cellSize = 1000
    searchRadius = 10000
    search=str(searchRadius/1000)

    # Execute KernelDensity
    outKernelDensity = KernelDensity(inFeatures, populationField, cellSize,
                                 searchRadius, "SQUARE_KILOMETERS")

    # Save the output
    outfile=outwk+"kernel_"+dataLabel+"_"+search+"km_"+fieldnames[i]+".tif"
    outKernelDensity.save(outfile)
