# Name: runKernelLoopSpecies.py
# Description: For a set of site x species shapefile, loop through shapefiles and grab nth column to produce kernel density rasters based on a given field and neighbourhood size
# Requirements: Spatial Analyst Extension
# Author: Katie Gale
# Date: 8 August 2017

# Import system modules
import arcpy
from arcpy import env
from arcpy.sa import *

# Set environment settings
arcpy.env.workspace = "E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/Shapefiles/Synoptics/totalBiomass"
inwk="E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/Shapefiles/Synoptics/totalBiomass/"
outwk="E:/Documents/Projects/EBSAs/Analyses/Shelf/KernelDensity/totalBiomass_Synoptic/10kmNeighbour_pooled/1.KernelDensity/"
arcpy.env.extent = arcpy.Extent(417800, 336000, 1142400, 1237800)

years= arcpy.ListFeatureClasses ()

def getFieldNames(shp):
        fieldnames = [f.name for f in arcpy.ListFields(shp)]
        return fieldnames

shape= arcpy.ListFeatureClasses ()
iteration=""

for i in xrange(0,len(years)):
    fieldnames = getFieldNames(inwk+years[i])
    # Set local variables
    inFeatures = inwk+years[i]
    populationField = fieldnames[3]
    cellSize = 1000
    searchRadius = 10000
    search=str(searchRadius/1000)

    # Execute KernelDensity
    outKernelDensity = KernelDensity(inFeatures, populationField, cellSize,
                                 searchRadius, "SQUARE_KILOMETERS")

    # Save the output
    outfile=outwk+"kernel_"+iteration+"_"+search+"km_"+years[i]+".tif"
    outname=outfile.replace(".shp.tif",".tif")
    outKernelDensity.save(outname)
