# Name: runKernel.py
# Description: Loops through point shapefiles and produces kernel density rasters based on a given field and neighbourhood size
# Requirements: Spatial Analyst Extension
# Author: Katie Gale
# Date: 2 August 2017

# Import system modules
import arcpy
from arcpy import env
from arcpy.sa import *

dataLabel="normCombine"

# Set environment settings
#env.workspace = "E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/LineFiles/Species/"
#inwk="E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/LineFiles/Species/"
#outwk="E:/Documents/Projects/EBSAs/Analyses/Shelf/KernelDensity/CPs_Synoptic/10kmNeighbour_line/1.KernelDensity/"

arcpy.env.workspace = "E:/Documents/Data files/MPATT Data/Groundfish/RescaleCombineThenKDE/MPATTFilesClip/Input"
inwk=arcpy.env.workspace+"/"
outwk=inwk.replace("Input","")+"1.KernelDensity/"

#arcpy.env.extent = arcpy.Extent(417800, 336000, 1142400, 1237800)
arcpy.env.extent = arcpy.Extent(459060, 1227650, 1087060, 539650) #PUs  #left,top,right,bottom

years= arcpy.ListFeatureClasses ()

def getFieldNames(shp):
        fieldnames = [f.name for f in arcpy.ListFields(shp)]
        return fieldnames

for i in xrange(0,len(years)):
    # Set local variables
    inFeatures = inwk+years[i]
    #populationField = "Hippoglo_1"
    #populationField = "LngcdRs"
    #populationField = "CtperHookp"
    #populationField = "WtperHr_To"
    populationField=getFieldNames(inwk+years[i])[3]
    cellSize = 1000
    searchRadius = 10000
    search=str(searchRadius/1000)

    # Execute KernelDensity
    outKernelDensity = KernelDensity(inFeatures, populationField, cellSize,
                                 searchRadius, "SQUARE_KILOMETERS")

    name=years[i]
    outname=name.replace(".shp",".tif")
    outfile=outwk+"kernel_"+dataLabel+"_"+search+"km_"+populationField+".tif"
    outKernelDensity.save(outfile)
