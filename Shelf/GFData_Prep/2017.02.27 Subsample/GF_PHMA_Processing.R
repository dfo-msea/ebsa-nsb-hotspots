#################################################################
# Formatting of survey data for community analysis
# PHMA Surveys - Pacific Halibut Management Association
# Katie Gale, 10 April 2017
# Edited 1 June 2017 and commited June 30
#   - in the long-form data, when calculating the sum of each species per tow, retain NAs (instead of converting to zero) to differentiate from true zero catch
#   - add wt and wt/hook/hr to output file, and drop just ct/hr for ct/hook/hr 
#   - ADDITIONAL edit 30 June, retain na.rm=T (negate above comment)
#
# 5 July 2017
# - add code to sum biomass of fish
#
# 14 July
# - add code to output a shapefile of just CPs
#
# 20 Sept
# - Get tracklines from start/end coordinates
####

library(data.table)
library(plyr)
library(reshape)
library(vegan)
library(dplyr)
library(lubridate)
library(sp)
library(rgdal)

#Start with phma Longline surveys
#Bring in Data from the GF Bio database
# phma<-data.table(read.csv("E:/Documents/Data files/Data Extractions and Sharing/Groundfish/GFBio_PFMA-Surveys_2017-04-10.txt"))
phma<-data.table(read.csv("E:/Documents/Data files/Data Extractions and Sharing/Groundfish/GFBio_PHMA-Surveys_HookCount_2017-04-18.txt")) #File with number of hooks 
head(phma) #20128
phma$FE_END_DEPLOYMENT_TIME<-as.character(phma$FE_END_DEPLOYMENT_TIME)

date<-as.POSIXct(phma$FE_END_DEPLOYMENT_TIME, format="%d/%m/%Y %H:%M:%S")
range(date, na.rm=T) #2006.08.05 to 2016.08.25
table(month(date))/201.28

phma$FE_SP<-paste(phma$FISHING_EVENT_ID, phma$SPECIES_CODE,sep="-") #Add ID number to keep track of individual records
phma[phma$FE_SP %in% phma$FE_SP[duplicated(phma$FE_SP)],][1:20] #Look at duplicate species per fishing event
length(unique(phma$FE_SP)) #19811 IDs
length(unique(phma$CATCH_ID)) #20128 - so there are about 1000 records of species weighed twice per tow 

unique(phma$GEAR_DESC) #all longline

#calculate Coordinates
phma$LatMid<-((phma$FE_START_LATTITUDE_DEGREE+(phma$FE_START_LATTITUDE_MINUTE/60))+(phma$FE_END_LATTITUDE_DEGREE+(phma$FE_END_LATTITUDE_MINUTE/60)))/2
phma$LonMid<-(-1)*((phma$FE_START_LONGITUDE_DEGREE+(phma$FE_START_LONGITUDE_MINUTE/60))+(phma$FE_END_LONGITUDE_DEGREE+(phma$FE_END_LONGITUDE_MINUTE/60)))/2

#create list of Fishing events and locations
FE_loc<-phma[,names(phma) %in% c("FISHING_EVENT_ID","LatMid","LonMid"), with=FALSE]
FE_loc<-FE_loc[!duplicated(FE_loc),] #1950 fishing events

#change start and end fishing dates into proper format
#To get duration, times must be in "POSIXct" format
phma$start.fishing<-as.POSIXct(phma$FE_END_DEPLOYMENT_TIME,format ="%d/%m/%Y %H:%M:%S")
phma$end.fishing<-as.POSIXct(phma$FE_BEGIN_RETRIEVAL_TIME,format ="%d/%m/%Y %H:%M:%S")

#Calculate duration of tow in seconds
phma$fishing.duration.s<-phma$end.fishing-phma$start.fishing; units(phma$fishing.duration.s)<-"secs"
str(phma$fishing.duration.s) #time in seconds
phma$fishing.duration.s<-as.numeric(phma$fishing.duration.s)
summary(phma$fishing.duration.s) #60-1120 seconds

#add month and year columns
phma$month<-month(phma$start.fishing)
phma$year<-year(phma$start.fishing)

#Drop those without fishing durations (meaning either start or end fishing times were not available)
phma2<-phma[!is.na(phma$fishing.duration.s),]
nrow(phma2) #20128 species records 

#start to calculate CPUE - depending on tow and species, some records are count and some are weight
summary(phma2$CATCH_WEIGHT) #18045 NAs - most of these records are not weighed!
summary(phma2$CATCH_COUNT) #5 NAs

phma2$FE_SP<-paste(phma2$FISHING_EVENT_ID, phma2$SPECIES_CODE, sep="-") #Create an identifier for each species-tow record
summarybytow<-phma2[,list(total_wt=sum(CATCH_WEIGHT, na.rm=T), total_count=sum(CATCH_COUNT, na.rm=T)), by=FE_SP] #sum weights and counts for each species-tow (some species were counted more than once per tow)
summarybytow[56:75] #19811 species records 
length(unique(summarybytow$FE_SP)) #19811 species-tow IDs

#Add the summary to the dataframe
phma3<-merge(phma2, summarybytow, by="FE_SP") #20128 
phma3<-subset(phma3, !duplicated(FE_SP)) #Remove duplicates (doesn't matter which one) - 19811
head(phma3)

#Look at catch values to see which records should be retained  
bothNA<-subset(phma3, (is.na(total_wt) & is.na(total_count))) #0 obs
bothZero<-subset(phma3, (total_wt==0 & total_count==0)) #check weights and counts that are both zero - 60 records
head(bothZero)
summary(bothZero$SPECIES_SCIENCE_NAME) #52/60 is unknown fish
bothZero[SPECIES_SCIENCE_NAME!="UNKNOWN FISH",] #these 8 records remaining are "CATCH ONLY" (verification code 9) and "COUNT, DOES NOT INCLUDE DROP-OFFS" (verification code 19), but they have no weight or count. 
# drop all these records

length(unique(phma3$FE_SP))

phma4<-phma3[!(phma3$FE_SP %in% bothZero$FE_SP),] #19751  

#Calculate CPUE Weight per Hour (kg/hr)
phma4$WTperHR<-phma4$total_wt/(phma4$fishing.duration.s/3600)
summary(phma4$WTperHR)
# phma4$WTperHR[phma4$WTperHR==Inf]<-0

#Calculate CPUE Count per Hour (#/hr)
phma4$CTperHR<-phma4$total_count/(phma4$fishing.duration.s/3600)
summary(phma4$CTperHR)
# phma4$CTperHR[phma4$CTperHR==Inf]<-0

plot(phma4$CTperHR)
plot(phma4$CTperHR,phma4$fishing.duration.s )
range(phma4$fishing.duration.s) #60-11220 seconds
points(phma4$CTperHR[phma4$fishing.duration.s<120],phma4$fishing.duration.s[phma4$fishing.duration.s<120], col="red")

#Calculate catch per hook per time 
range(phma4$LGLSP_HOOK_COUNT) #245-514 "Number of hooks actually fished on the longline during an event"
phma4$CtperHookperHR<-phma4$total_count/phma4$LGLSP_HOOK_COUNT/(phma4$fishing.duration.s/3600)

#Calculate weight per hook per time 
phma4$WtperHookperHR<-phma4$total_wt/phma4$LGLSP_HOOK_COUNT/(phma4$fishing.duration.s/3600)

#drop points with fishing duration < 180
phma4<-phma4[phma4$fishing.duration.s> 180,] #19735

#Compare with and without hooks
plot(phma4$CtperHookperHR,phma4$CTperHR )
plot(phma4$CTperHR)
plot(phma4$CtperHookperHR)

#Subset columns to be more manageable
phmaDat<-phma4[,names(phma4) %in% c("FE_SP","TRIP_ID","ACTIVITY_CODE","TRIP_START_DATE","year", "month","FISHING_EVENT_ID",
                                    "GEAR_DESC","LatMid","LonMid","start.fishing","end.fishing","fishing.duration.s","LGLSP_HOOK_COUNT",
                                    "SPECIES_CODE","SPECIES_SCIENCE_NAME","total_wt","total_count","WtperHookperHR","CtperHookperHR" ), with=F]

#Check records that don't have science names
phmaDat$SPECIES_CODE[phmaDat$SPECIES_SCIENCE_NAME==""] #None

##Fix names
#Bring in SpeciesNames from WoRMS 
specNames<-read.delim("E:/Documents/Data files/Taxonomy/Taxonomy.txt",sep="\t")
specNames<-specNames[,names(specNames) %in% c("ScientificName","ScientificName_accepted","Phylum","Class","Order")]
specNames<-unique(specNames) 
names(specNames)[1:2]<-c("OriginalName","ValidName")

#Make both original and WoRMS lookup names lowercase to match
specNames$OriginalName<-tolower(specNames$OriginalName)
phmaDat$OriginalName<-tolower(phmaDat$SPECIES_SCIENCE_NAME)
unique(phmaDat$OriginalName)

#join corrected names
nrow(phmaDat) #19735
length(unique(phmaDat$FE_SP)) #19735

#change blackspotted to rougheye to make rougheye-blackspotted 
phmaDat$OriginalName[phmaDat$SPECIES_CODE==425]<-"sebastes aleutianus"
phmaDat$OriginalName[phmaDat$SPECIES_CODE==394]<-"sebastes aleutianus"

#fix a spelling
phmaDat$OriginalName[phmaDat$SPECIES_CODE=="5HI"]<-"ophiura scutellata"

phmaDat2<-merge(as.data.frame(phmaDat), specNames, by="OriginalName", all.x=T, all.y=F) #19735 #fixed merge error with duplicate names

#Look at NAs and blanks
unique(phmaDat2$OriginalName[is.na(phmaDat2$ValidName)]) #Acceptable to leave these as NA - remove them
phmaDat2$ValidName<-as.factor(phmaDat2$ValidName)
unique(phmaDat2$OriginalName[is.na(phmaDat2$ValidName)])
unique(phmaDat2$OriginalName[phmaDat2$ValidName==""])  #No records without valid names

#remove NA records
phmaDat3<-data.table(phmaDat2[!is.na(phmaDat2$ValidName),]) #19722
phmaDat3<-data.table(phmaDat3[phmaDat3$ValidName!="",]) #19722

nrow(phmaDat3) #19722
length(unique(phmaDat3$FE_SP)) #19722
phmaDat3[phmaDat3$FE_SP %in% phmaDat3$FE_SP[duplicated(phmaDat3$FE_SP)],]

phmaDat3[phmaDat3$ValidName==""] #check that that none of the names are blank

phmaDat3<-phmaDat3[,!(names(phmaDat3) %in% c("OriginalName","SPECIES_SCIENCE_NAME")),with=FALSE] #Remove these 2 columns

#Bring in file that has taxon level for each name
taxonLevel<-read.csv("E:/Documents/Data files/Taxonomy//Species_TaxonLevel.csv")
names(taxonLevel)<-c("ValidName","TaxonLevel")
taxonLevel<-unique(taxonLevel)

#Add taxonlevel to the dataframe
phmaDat4<-merge(phmaDat3,taxonLevel, by="ValidName", all.x=T, all.y=F )
nrow(phmaDat4) #19722
phmaDat4$TaxonLevel<-tolower(phmaDat4$TaxonLevel)

#Order columns
setcolorder(phmaDat4, c("FISHING_EVENT_ID","TRIP_ID","FE_SP","ACTIVITY_CODE","year", "month",
                        "GEAR_DESC","LatMid","LonMid","start.fishing","end.fishing","fishing.duration.s","LGLSP_HOOK_COUNT",
                        "SPECIES_CODE","ValidName","Phylum","Class","Order","TaxonLevel","total_wt","total_count","WtperHookperHR","CtperHookperHR"))

subset(phmaDat4, is.na(TaxonLevel)) #check for no level - none
names(phmaDat4)
length(unique(phmaDat4$ValidName)) #148 taxa

#Create object with only species-level records
phmaDatSpecies<-subset(phmaDat4, TaxonLevel=="species") #18142 records are to species only
nrow(phmaDatSpecies)
length(unique(phmaDat4$ValidName[(phmaDat4$FE_SP %in% phmaDatSpecies$FE_SP)])) #94 species
nrow(phmaDat4) #19722
# 
write.csv(phmaDat4, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/PHMA/GFPHMA_AllTaxa_19722_2017.06.30.csv",row.names=F)
write.csv(phmaDatSpecies, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/PHMA/GFPHMA_Species_18142_2017.06.30.csv",row.names=F)

##########
#Grid data
########
library(maptools)
library(rgdal)

#Start with the dataframe of species records
phmaDat5<-data.table(phmaDat4)
head(phmaDat5)

#Add duplicate columns for Lat and Long - helps later
phmaDat5$LatMid1<-phmaDat5$LatMid
phmaDat5$LonMid1<-phmaDat5$LonMid

#Turn this dataframe into a spatial object
coordinates(phmaDat5)<-~LonMid+LatMid #tell R what the coordates are
proj4string(phmaDat5)<-CRS("+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs") #set coordinate reference system
DatShp <- spTransform(phmaDat5, CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")) #project

#import 1 km planning grid and set projection
PUgrid<-readShapeSpatial("E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Inputs/MaPP_PU_Final/MaPP_PU_Final.shp")
proj4string(PUgrid)<-CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs") #project

####
# spatial join of trawl points with 1km grid - overlay could take a long time
#####
overlay<-over(DatShp,PUgrid) #Spatial join our dataset with the grids
DatwGrid<-cbind(DatShp@data,overlay ) #Bind the species data with the overlay info 

DatwGrid<-DatwGrid[,!(names(DatwGrid) %in% c("OBJECTID_1","Shape_Le_1","Shape_Area")) ] #drop unwanted columns from grid shapefile
head(DatwGrid)

#Create a shapefile that has just the PU grids that contain data
#if gridID=NA, then that point was outside of our study area
populated<-(unique(DatwGrid$PU_ID)) #get list of populated grid cells
length(unique(PUgrid$PU_ID)) #113839 grid cells initiallly 
gridSubAllTaxa<-subset(PUgrid,PUgrid$PU_ID %in% populated) #select only those grid cells that our data populates
length(unique(gridSubAllTaxa$PU_ID)) #   1499 grid cells are populated by all taxa 
head(gridSubAllTaxa@data)

#Write this shapefile for later use
# writeSpatialShape(gridSubAllTaxa,"E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/PU_with_Data_PHMA_2017.04.10.shp") 

DatwGrid2<-DatwGrid[!is.na(DatwGrid$PU_ID),] #Drop records that are outside the planning region (16525 records remain)
nrow(DatwGrid2)

#Look at planning units that have multiple surveys in them
#Select planning units that have more than 1 fishing event

PU_FE<-cbind.data.frame(DatwGrid2$FISHING_EVENT_ID,DatwGrid2$PU_ID)
PU_FE<-unique(PU_FE)
names(PU_FE)<-c("FISHING_EVENT_ID","PU_ID")
head(PU_FE) #all fishing events with associated planning unit

PUCount<-ddply(PU_FE,  c("PU_ID"), summarize,nFEinPU=length(unique(FISHING_EVENT_ID)))
head(PUCount)

#Output - number of fishing evnets per planning unit
PUGridCount<-merge(PUgrid,PUCount,  by="PU_ID")
head(PUGridCount@data)
PUGridCount@data$nFEinPU[is.na(PUGridCount@data$nFEinPU)]<-0
# writeSpatialShape(PUGridCount, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/PHMA_NFEperPU_2017,04,18.shp")

dupPU<-subset(PUCount, nFEinPU>1) #select planning units that have more than one fishing event in them
dupPU  #94 planning units have more than one fishing event (of 1499)

#randomly select one FISHING_EVENT from each duplicated PU

# ### !!!
# # ##NOTE EVERYTIME THIS IS RUN, A DIFFERENT RESULT IS OBTAINED WHICH CHANGES THE N SPECIES
# # ### !!!
# subs<-PU_FE %>% group_by(PU_ID) %>% sample_n(size = 1) #Select one Fishing event per planning unit
# head(subs)
# subs$FISHING_EVENT_ID
# 
# DatwGrid2Subset<-DatwGrid2[DatwGrid2$FISHING_EVENT %in% subs$FISHING_EVENT_ID,] #subset the dataset to include only those fishing events selected above
# head(DatwGrid2Subset )
# nrow(DatwGrid2Subset ) #15563
# # write.csv(DatwGrid2Subset, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/GFPHMA_Subsetted_AlltaxaInPU_15563_2017.04.18.csv",row.names=F)
# 
# ###
# START HERE AFTER SUBSETTING TO AVOID OVERWRITING RANDOMIZATION


################################
#TOW X SPECIES Matrices
################################
DatwGrid2Subset<-read.csv("E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/PHMA/GFPHMA_Subsetted_AlltaxaInPU_15563_2017.04.18.csv")
head(DatwGrid2Subset)
# phmaDat4
# head(phmaDat4) #bring in the current file and see if the values for the fishing events match
# phmaDat4SUB<-phmaDat4[phmaDat4$FISHING_EVENT_ID %in% DatwGrid2SubsetOLD$FISHING_EVENT_ID, ]
# head(phmaDat4SUB)
# nrow(phmaDat4SUB)
# nrow(DatwGrid2SubsetOLD) #The old one has fewer records. Which ones are missing?
# length(unique(phmaDat4SUB$FE_SP)) #15585
# length(unique(DatwGrid2SubsetOLD$FE_SP)) #15563
# 
# phmaDat4SUB$CtperHookperHR[!phmaDat4SUB$FE_SP %in% DatwGrid2SubsetOLD$FE_SP,  ] #The new one has this unknown fish. This is only going to make a difference for the total biomass analysis. 
# phmaDat4SUB[phmaDat4SUB$SPECIES_CODE=="015",]
# phmaDat4SUB2<-phmaDat4SUB[phmaDat4SUB$SPECIES_CODE!="015",] #15563
# length(unique(phmaDat4SUB2$FE_SP)) #15563
# 
# sum(phmaDat4SUB$CtperHookperHR)
# 
# #check if the values for the 
# fes<-phmaDat4SUB$FE_SP
# summary<-list()
# for (i in 1:length(fes)){
# test<-fes[i]
# summary[[i]]<-print(as.character(phmaDat4SUB$CtperHookperHR[phmaDat4SUB$FE_SP==test]) == as.character(DatwGrid2SubsetOLD$CtperHookperHR[DatwGrid2SubsetOLD$FE_SP==test]))
# }
# list<-do.call(rbind.data.frame, summary)
# summary(list) #all values match. Okay

head(DatwGrid2Subset)
DatwGrid2Subset$start.fishing<-as.POSIXct(DatwGrid2Subset$start.fishing)
range(DatwGrid2Subset$start.fishing)
table(DatwGrid2Subset$month)/155.63
nrow(DatwGrid2Subset) #15563
length(unique(DatwGrid2Subset$ValidName)) #137 taxa

FELoc<-unique(cbind.data.frame(DatwGrid2Subset$FISHING_EVENT_ID, DatwGrid2Subset$LatMid1, DatwGrid2Subset$LonMid1, DatwGrid2Subset$ACTIVITY_CODE,DatwGrid2Subset$year, DatwGrid2Subset$month) ) #fishing event locations
names(FELoc)<-c("FISHING_EVENT_ID","LatMid","LonMid","ACTIVITY_CODE" ,"Year", "Month")
head(FELoc)

# #Tow by species count
perSetCt<-DatwGrid2Subset[,c("FISHING_EVENT_ID","ValidName","CtperHookperHR")]
head(perSetCt)
perSetCtMelt<-melt(perSetCt, id.vars=c("FISHING_EVENT_ID","ValidName"))
perSetCtCast<-cast(perSetCtMelt, FISHING_EVENT_ID~ValidName, fun=sum) #site x species
dim(perSetCtCast) #1499 x 137 taxa + 1 col
perSetCtCast2<-join(perSetCtCast, FELoc, by="FISHING_EVENT_ID") #attach locations
dim(perSetCtCast2) #1499 x 137 taxa + 3 col
# write.csv(perSetCtCast2, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/PHMA/GFPHMA_Subsampled_TowxCtHook_TaxaInPU_2017.04.18.csv",row.names=F)


#GET FISHES
fishes<-DatwGrid2Subset[DatwGrid2Subset$Class %in% c("Actinopteri","Elasmobranchii","Myxini","Holocephali","Petromyzonti")|DatwGrid2Subset$ValidName %in% c("Gnathostomata","Pisces"),]
length(unique(fishes$ValidName[fishes$TaxonLevel=="species"])) #69
fishNames<-unique(fishes$ValidName)
fishSpNames<-unique(fishes$ValidName[fishes$TaxonLevel=="species"])
fishSpNames<-c(paste(fishSpNames), "Sebastes diaconus")
fishSpNames #70

perSetCtFISH<-DatwGrid2Subset[,c("FISHING_EVENT_ID","ValidName","CtperHookperHR")]
perSetCtFISH<-perSetCtFISH[perSetCtFISH$ValidName %in% fishSpNames,] #13731
unique(perSetCtFISH$ValidName) #70 species
head(perSetCtFISH)
perSetCtMeltFISH<-melt(perSetCtFISH, id.vars=c("FISHING_EVENT_ID","ValidName"))
perSetCtCastFISH<-cast(perSetCtMeltFISH, FISHING_EVENT_ID~ValidName, fun=sum) #site x species
dim(perSetCtCastFISH) #1499 x 70 taxa + 1 col
perSetCtCast2FISH<-join(perSetCtCastFISH, FELoc, by="FISHING_EVENT_ID") #attach locations
dim(perSetCtCast2FISH) #1499 x 70 taxa + 3 col

FishMatrix<-perSetCtCast2FISH
FishMatrixSpec<-perSetCtCast2FISH[,c(-1,-72,-73)]

#FishSummary
summaryFish<-cbind.data.frame(FISHING_EVENT_ID=FishMatrix$FISHING_EVENT_ID,
                              nSp=specnumber(FishMatrixSpec),
                              Shan=diversity(FishMatrixSpec,index="shannon"),
                              J_Pielou=(diversity(FishMatrixSpec,index="shannon")/log(specnumber(FishMatrixSpec))),
                              ExpShan=exp(diversity(FishMatrixSpec,index="shannon")),
                              E_Heip=((exp(diversity(FishMatrixSpec,index="shannon"))-1)/(specnumber(FishMatrixSpec)-1)),                              
                              LatMid=FishMatrix$LatMid,LonMid=FishMatrix$LonMid)

head(summaryFish)
# write.csv(summaryFish, "E:/Documents/Projects/NearshoreEBSA//GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/PHMA/GFPHMA_Subsetted_diversitySummaryFish_2017.04.18.csv",row.names=F)

summaryFish<-read.csv("E:/Documents/Projects/NearshoreEBSA//GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/PHMA/GFPHMA_Subsetted_diversitySummaryFish_2017.04.18.csv")

coordinates(summaryFish)<-~LonMid+LatMid
fields<-c("nSp","Shan","J_Pielou")

for (i in 1:3){
  subset<-summaryFish[,names(summaryFish) %in% c("FISHING_EVENT_ID",paste(fields[i]),"LatMid", "LonMid")]
  head(subset)
  writeSpatialShape(subset, paste("E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/PHMA/GFPHMA_Subsample_DiversityFISH_CountHrHook_",fields[i],".shp",sep=""))
}

#Total Weight for all fish (all taxa)
perSetCtFISH<-DatwGrid2Subset[,c("FISHING_EVENT_ID","ValidName","CtperHookperHR")]
perSetCtFISHall<-perSetCtFISH[perSetCtFISH$ValidName %in% fishNames,] #13780
unique(perSetCtFISHall$ValidName) #79 taxa
head(perSetCtFISHall)
perSetCtMeltFISHall<-melt(perSetCtFISHall, id.vars=c("FISHING_EVENT_ID","ValidName"))
perSetCtCastFISHall<-cast(perSetCtMeltFISHall, FISHING_EVENT_ID~ValidName, fun=sum) #site x species
dim(perSetCtCastFISHall) #1499 x 79 taxa + 1 col
perSetCtCast2FISHall<-join(perSetCtCastFISHall, FELoc, by="FISHING_EVENT_ID") #attach locations
dim(perSetCtCast2FISHall) #1499 x 79 taxa + 6 col


FishMatrixAllTaxa<-perSetCtCast2FISHall[,names(perSetCtCast2FISHall) %in% c(paste(fishNames),"LatMid","LonMid","Year","Month","ACTIVITY_CODE","FISHING_EVENT_ID")]
head(FishMatrixAllTaxa)
FishMatrixAllTaxaTaxa<-FishMatrixAllTaxa[2:(ncol(FishMatrixAllTaxa)-5)] #get taxa data only from here (drop FE and lat long and dates)
head(FishMatrixAllTaxaTaxa)
FishBiomass<-cbind.data.frame(FISHING_EVENT_ID=FishMatrixAllTaxa$FISHING_EVENT_ID,CtperHookperHr_Total=rowSums(FishMatrixAllTaxaTaxa),
                              LatMid=FishMatrixAllTaxa$LatMid,LonMid=FishMatrixAllTaxa$LonMid)
head(FishBiomass)
write.csv(FishBiomass, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/PHMA/GFPHMA_Subsampled_totalBiomassFish_2017.07.05.csv")




#######
#Shapefile Creation
#########
library(rgdal)
library(maptools)
library(sp)

perSetCtCast2<-read.csv("E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/Textfiles/PHMA/GFPHMA_Subsampled_TowxCtHook_TaxaInPU_2017.04.18.csv")
alldat<-read.csv("E:/Documents/Projects//EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/Textfiles/PHMA/GFPHMA_Subsetted_AlltaxaInPU_15563_2017.04.18.csv")
year<-unique(cbind.data.frame(FISHING_EVENT_ID=alldat$FISHING_EVENT_ID, Year=alldat$year))

perSetCtCast2<-merge(perSetCtCast2, year, by="FISHING_EVENT_ID")
head(perSetCtCast2)

#Shapefile of Years
#Separate TowXWt Matrix by year and make shapefiles and text files
dat<-unique(data.frame(year=perSetCtCast2$Year)) #, surv=perSetWtSUBCast2$ACTIVITY_CODE))
# for (i in 1:nrow(dat)){
#   sub<-perSetCtCast2[perSetCtCast2$Year==dat[i,1],] #&perSetWtSUBCast2$ACTIVITY_CODE==dat[i,2],]
#   sub$lat<-sub$LatMid
#   sub$lon<-sub$LonMid
#   coordinates(sub)<-~lon+lat
#   proj4string(sub)<-CRS("+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs") #set coordinate reference system
#   subshp <- spTransform(sub, CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")) #project
#   writeOGR(subshp, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/PHMA/TowxCtByYear",layer=paste("/PHMA_Sub_TowxCt_",
#                                                                                                    #dat[i,2],"_",
#                                                                                                    dat[i,1], sep=""),driver="ESRI Shapefile" )
#   db<-read.dbf(paste("E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/PHMA/TowxCtByYear/PHMA_Sub_TowxCt_",
#                      #dat[i,2],"_",
#                      dat[i,1],".dbf",sep=""))
#   head(db)
#   names(db)<-names(sub)
#   head(db)
#   write.dbf(db,paste("E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/PHMA/TowxCtByYear/PHMA_Sub_TowxCt_",
#                      #dat[i,2],"_",
#                      dat[i,1],".dbf",sep="") )
# }


#Shapefile of just CPs
cp<-read.delim("E:/Documents/Projects/MPAT/3.Conservation Priorities/3.Screen/CPList.txt", sep="\t")
cp$ValidName<-gsub("\\s",".",cp$ValidName)
head(cp)

perSetCtCast2<-perSetCtCast2[,names(perSetCtCast2) %in% c(cp$ValidName,"FISHING_EVENT_ID","LonMid","LatMid","Year","Month","ACTIVITY_CODE")]
names(perSetCtCast2)
perSetCtCast2$Lat<-perSetCtCast2$LatMid
perSetCtCast2$Lon<-perSetCtCast2$LonMid
coordinates(perSetCtCast2)<-~Lon+Lat
proj4string(perSetCtCast2)<-CRS("+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs") #set coordinate reference system
perSetCtCast2 <- spTransform(perSetCtCast2, CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")) #project
head(perSetCtCast2)
names(perSetCtCast2)<- cp$Code[match(names(perSetCtCast2), cp$ValidName)]
names(perSetCtCast2)
names(perSetCtCast2)[c(1,(ncol(perSetCtCast2)-2):ncol(perSetCtCast2))]<-c("FISHING_EVENT_ID","LonMid","LatMid","Year")
names(perSetCtCast2) <- strtrim(names(perSetCtCast2),10)
writeOGR(perSetCtCast2, "E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/Shapefiles/PHMA/CPs",
         layer=paste("GFPHMA_Subsampled_TowxCtHook_TaxaInPU_CPs"),driver="ESRI Shapefile",overwrite=T)

GFPHMAshp<-perSetCtCast2
coordinates(GFPHMAshp)<-~LonMid+LatMid

# #Do for Tows
# #Layers for For individual species
# Sable<-c("FISHING_EVENT_ID","PU_ID","Anoplopoma.fimbria","LatMid", "LonMid")
# Sable_tow<-GFPHMAshp[,(names(GFPHMAshp) %in% Sable)]
# head(Sable_tow)
# writeSpatialShape(Sable_tow, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/PHMA/GFPHMA_CtHookTow_Sablefish.shp")
# 
# Halibut<-c("FISHING_EVENT_ID","PU_ID","Hippoglossus.stenolepis","LatMid", "LonMid")
# Halibut_tow<-GFPHMAshp[,(names(GFPHMAshp) %in% Halibut)]
# head(Halibut_tow)
# writeSpatialShape(Halibut_tow, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/PHMA/GFPHMA_CtHookTow_Halibut.shp")
# 
# Yelloweye<-c("FISHING_EVENT_ID","PU_ID","Sebastes.ruberrimus","LatMid", "LonMid")
# Yelloweye_tow<-GFPHMAshp[,(names(GFPHMAshp) %in% Yelloweye)]
# head(Yelloweye_tow)
# writeSpatialShape(Yelloweye_tow, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/PHMA/GFPHMA_CtHookTow_Yelloweye.shp")
# 
# Lingcod<-c("FISHING_EVENT_ID","PU_ID","Ophiodon.elongatus","LatMid", "LonMid")
# Lingcod_tow<-GFPHMAshp[,(names(GFPHMAshp) %in% Lingcod)]
# head(Lingcod_tow)
# writeSpatialShape(Lingcod_tow, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/PHMA/GFPHMA_CtHookTow_Lingcod.shp")
# 
# Pcod<-c("FISHING_EVENT_ID","PU_ID","Gadus.macrocephalus","LatMid", "LonMid")
# Pcod_tow<-GFPHMAshp[,(names(GFPHMAshp) %in% Pcod)]
# head(Pcod_tow)
# writeSpatialShape(Pcod_tow, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/PHMA/GFPHMA_CtHookTow_Pcod.shp")
# 
# Arrowtooth<-c("FISHING_EVENT_ID","PU_ID","Atheresthes.stomias","LatMid", "LonMid")
# Arrowtooth_tow<-GFPHMAshp[,(names(GFPHMAshp) %in% Arrowtooth)]
# head(Arrowtooth_tow)
# writeSpatialShape(Arrowtooth_tow, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/PHMA/GFPHMA_CtHookTow_Arrowtooth.shp")

###
#Get LINES shapefile
phma1<-phma
phma1$LatStart<-phma1$FE_START_LATTITUDE_DEGREE+(phma1$FE_START_LATTITUDE_MINUTE/60)
phma1$LatEnd<-phma1$FE_END_LATTITUDE_DEGREE+(phma1$FE_END_LATTITUDE_MINUTE/60)
phma1$LonStart<-(-1)*(phma1$FE_START_LONGITUDE_DEGREE+(phma1$FE_START_LONGITUDE_MINUTE/60))
phma1$LonEnd<-(-1)*(phma1$FE_END_LONGITUDE_DEGREE+(phma1$FE_END_LONGITUDE_MINUTE/60))
head(phma1)

linea<-as.data.frame(unique(phma1[,c("FISHING_EVENT_ID","LatStart","LatEnd","LonStart","LonEnd")]))
ranlines <- list() 
for (irow in 1:nrow(linea)) { 
  ranlines[[irow]] <- Lines(Line(rbind(as.numeric(linea[irow, c("LonStart", 
                                                                "LatStart")]), as.numeric(linea[irow, c("LonEnd", "LatEnd")]))), 
                            ID = as.character(irow)) 
} 
sldf <- SpatialLinesDataFrame(SpatialLines(ranlines), linea, match.ID = FALSE) 



proj4string(sldf)<-CRS("+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs") #set coordinate reference system
sldf <- spTransform(sldf, CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")) #project
writeOGR(sldf, dsn="E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/LineFiles/PHMA", layer="PHMAFELines", driver="ESRI Shapefile")
nrow(sldf) #1950 FEs
library(rgeos)
summary(gLength(sldf, byid=TRUE))

#attach CP data
phmaCP<-read.dbf("E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/Shapefiles/PHMA/CPs/GFPHMA_Subsampled_TowxCtHook_TaxaInPU_CPs.dbf")
names(phmaCP)[1]<-"FISHING_EVENT_ID"
phmaLines<-merge(sldf,phmaCP, by="FISHING_EVENT_ID", all.y=T, all.x=F)
names(phmaLines) <- strtrim(names(phmaLines),10)
writeOGR(phmaLines, dsn="E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/LineFiles/PHMA/Species", layer="PHMAFELinesSp", driver="ESRI Shapefile",overwrite=T)

