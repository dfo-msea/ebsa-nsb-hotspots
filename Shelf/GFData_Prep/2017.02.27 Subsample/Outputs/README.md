# Files for kernel density maps
2017.02.28
Updated 2017.04.18

# Groundfish Synoptic Surveys
GFBio, "fully usable" records, synoptic surveys only, for the area of the Northern Shelf Bioregion. One rex sole record (fishing event 2535929) removed.

Notes:
* Multiple weights per tow (fishing event) were summed for each species
* Species with zero weight or count were retained (mostly "trace amount"). These won't be used for the weight matrix, but will inform the presence-absence matrix.


## Files delivered 28 Feb 2017
### Textfiles:
**AllTaxa_** and **Species_8800**
* Cleaned-up records out of GFBio, with higher-level taxonomic classification appended (long-mode)

**Subsampled_AllTaxainPu_68811**
* Cleaned up records, randomly subsetted such that only one fishing_event occurred in each 1 km planning unit, within the planning region (long-mode)

**Subsampled_TowxPres_AllTaxa** (3279 tows x 718 taxa) and **_Species** (3279 tows x 444 species)
* Subsampled records, converted into presence-absence matrices of all taxon (or all species-level taxa) occuring in each tow, within the planning region.
* Includes species that were not weighed or counted (mostly "trace" records)

**Subsampled_TowxWt_AllTaxa** (3279 tows x 718 taxa) and **_Species** (3279 tows x 444 species)
* Subsampled records, converted into tow x taxon matrix, where values are sum of all weights for that each taxon (or each species-level taxon) within each tow, within the planning region.

**Subsampled_nSpperTow**, **_nSpperTowFish**, **_nSpperTowInverts**
* Subsampled records, number of species-level taxa per tow for PRESENCE records for all taxa, fishes, and inverts
* Includes species that were not weighed or counted (mostly "trace" records)

**Subsampled_TotalWtPerHr_AllTaxa**
* Subsampled records, total weight/hr across all taxa per tow


### Shapefiles (GFSynopticSubsample_):

**Arrowtooth, Bocaccio, Halibut, Lingcod, Pcod, Rougheye, Sablefish, Shortraker, Walleye, Yelloweye**
* Weight/hr for each species from each tow.

**RichnessPRESENCE-allspp, -fish, -invert**
* number of species in each tow for all species-level taxa recorded, and for fish and inverts separately, including trace species

**ShannonWEIGHT**
* Shannon index for tows, for species-level taxa that had weights (i.e., not the trace species).

**RichnessWEIGHT**
* number of species in each tow for species-level taxa that had weights (i.e., not the trace species).

**TotalWtPerHour_AllTaxa**
* total weight/hr for all taxa in each tow.

## Additional files delivered 16 March 2017

**Subsampled_DiversityFISH_WEIGHT_** and **DiversityINVERT_WEIGHT_E_Heip**, **ExpShan**, **J_Pielou**, **Shan**, and **nSp**
* Diversity metrics for each tow, for species-level taxa of fish and inverts, for records that had weights recorded
* nSp calcuated with `specnumber()` in vegan
* Shannon calculation from `diversity()` in vegan is the base of the other 3 metrics. ExpShan is `exp(shan)`; Pielou is `shan/log(nSp)`; and Heip is `[exp(shan)-1]/[nSp-1]`

## Additional files 7 April 2017
**NFEperPU.shp**
* Number of fishing events per planning unit, for all MaPP planning units

# IPHC and PHMA Surveys 
## Additional files 10 April 2017
#### Textfiles
**IPHC_AllTaxa_18012** and **IPHC_Species_16657**
**PHMA_AllTaxa_19697** and **PHMA_Species_18142**
* Cleaned-up records out of GFBio, with higher-level taxonomic classification appended (long-mode)

**PUxCt_AllTaxa and **PUxCtSD_AllTaxa** for IPHC
* Average and SD of Counts/hr of species per planning unit

**TowxCt_SpeciesinPU** for IPHC and PHMA
* Count/hr of species (or all taxa? check) by fishing event (tow), for records that occur in the NSB

**PHMA_Subsetted_AlltaxaInPU_15548** for PHMA
* Cleaned up records, randomly subsetted such that only one fishing_event occurred in each 1 km planning unit, within the planning region (long-mode)

#### Shapefiles
**PHMA_CtTow_Halibut, Sablefish, Yelloweye**
* Count/hr of species per fishing event from PHMA. 

**IPHC_CtTow_Halibut, POP, Redstripe, Sablefish, Shortspine, Yelloweye, Yellowmouth**
* Count/hr of species per fishing event from IPHC

**IPHC_CtAvg and CtSD_PU_Halibut, POP, Redstripe, Sablefish, Shortspine, Yelloweye, Yellowmouth**
* Average and SD of count/hr of each species within each Planning Unit

**NFEperPU for PHMA and IPHC**
* Number of fishing events per 1 km planning unit

**PU_with_Data_PHMA, _IPHC**
* shapfile of inhabited planning units for each data set

## Additional files 18 Apr
**PHMA_CtHookTow_Halibut, Sablefish, Yelloweye, Lingcod, PCod, Arrowtooth** (18 April 2017)
* Count/hook/hr of species per fishing event from PHMA. Replaces the Count/hr files above. 

**PHMA_Subsample_DiversityFISH_CountHrHook_J_Pielou**, **Shan**, and **nSp**
* Diversity metrics for each tow, for species-level taxa of fish and inverts, for records that had weights recorded
* nSp calcuated with `specnumber()` in vegan
* Shannon calculation from `diversity()` in vegan is the base of the other metric.  Pielou is `shan/log(nSp)`

## Additional files 30 June
**Synoptic_Subsampled_totalBiomassFish and totalBiomassInverts**
* Total biomass (wt/hr) summed across all taxa (species- and non-species level) per tow
