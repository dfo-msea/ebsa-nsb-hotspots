# 2020-01-21 --- These scripts are deprecated and are here for archive purposes only

## Groundfish Data for EBSA Project

Katie Gale 28 Feb 2017, updated 16 March 2017, 10 April 2017

## Data source
Data was queried from GFBIO on 28 November 2016, for all synoptic surveys with usability code "fully usable"
- updated synoptic query was run on 9 March 2017
- queries for Sablefish, IPHC, and PHMA surveys were run on 9 Mar and 10 Apr

## Processing
* Data were subsampled such that only one tow occurred in each 1 km planning unit
* Species with catch verification codes 7 and 12 (trace amount, or not weighed/counted) were retained as "presence" records but did not count towards weight values
* Counts and weights for species with multiple records per tow were summed before calculating CPUE
* One rex sole record (fishing event 2535929) removed.

## Outputs 
* Site x species matrices for
 * Sites = Planning units or tows
 * Species = All taxa, species-level taxa only, species-level fishes only, species-level invertebrates only
 * Values = Present, total count/hr or weight/hr 
* Shapefiles for richness for species-level taxa per tow
 * Number of species per tow for presence records
 * Number of Species per tow for species with weights only
* Shapefiles for Shannon-Weiner Diversity per tow for species with weights only
* Total weight per hour for all species per tow, including "Animalia" (catch not ID'd, just weighed)
* Shapefiles for weight/hr per tow for individual species
* Shapefiles for diversity metrics (number of species, shannon, exponential shannon, pielou's, and heip's) for species-level records with weights recorded, for fish and invertebrates separately
* Shapefile for number of fishing events per planning unit

*Summaries of total biomass (wt/hr) of all taxa for fishes and inverts per tow

## SQL Code for initial query 28 Nov 2016
`SELECT TRIP.TRIP_ID, TRIP.TRIP_START_DATE, TRIP.TRIP_END_DATE, TRIP_ACTIVITY.ACTIVITY_CODE, FISHING_EVENT.FISHING_EVENT_ID, GEAR.GEAR_DESC, FISHING_EVENT.FE_BEGIN_DEPLOYMENT_TIME, FISHING_EVENT.FE_END_DEPLOYMENT_TIME, FISHING_EVENT.FE_BEGIN_RETRIEVAL_TIME, FISHING_EVENT.FE_END_RETRIEVAL_TIME, FISHING_EVENT.FE_BEGIN_BOTTOM_CONTACT_TIME, FISHING_EVENT.FE_END_BOTTOM_CONTACT_TIME, FISHING_EVENT.FE_START_LATTITUDE_DEGREE, FISHING_EVENT.FE_START_LATTITUDE_MINUTE, FISHING_EVENT.FE_END_LATTITUDE_DEGREE, FISHING_EVENT.FE_END_LATTITUDE_MINUTE, FISHING_EVENT.FE_START_LONGITUDE_DEGREE, FISHING_EVENT.FE_START_LONGITUDE_MINUTE, FISHING_EVENT.FE_END_LONGITUDE_DEGREE, FISHING_EVENT.FE_END_LONGITUDE_MINUTE, FISHING_EVENT_CATCH.CATCH_ID, CATCH.SPECIES_CODE, CATCH.SPECIES_CATEGORY_CODE, CATCH.CATCH_WEIGHT, CATCH.CATCH_COUNT, CATCH.CATCH_WEIGHT_PER_PIECE, CATCH.CATCH_MIN_SORT_LENGTH, CATCH.CATCH_MAX_SORT_LENGTH, CATCH.CATCH_VERIFICATION_CODE, CATCH.CATCH_COMMENT, SPECIES.SPECIES_SCIENCE_NAME, USABILITY.USABILITY_DESC
FROM USABILITY INNER JOIN ((GEAR INNER JOIN (SPECIES INNER JOIN (CATCH INNER JOIN (((TRIP INNER JOIN TRIP_ACTIVITY ON TRIP.TRIP_ID = TRIP_ACTIVITY.TRIP_ID) INNER JOIN FISHING_EVENT ON TRIP.TRIP_ID = FISHING_EVENT.TRIP_ID) INNER JOIN FISHING_EVENT_CATCH ON FISHING_EVENT.FISHING_EVENT_ID = FISHING_EVENT_CATCH.FISHING_EVENT_ID) ON CATCH.CATCH_ID = FISHING_EVENT_CATCH.CATCH_ID) ON SPECIES.SPECIES_CODE = CATCH.SPECIES_CODE) ON GEAR.GEAR_CODE = FISHING_EVENT.GEAR_CODE) INNER JOIN TRAWL_SPECS ON FISHING_EVENT.FISHING_EVENT_ID = TRAWL_SPECS.FISHING_EVENT_ID) ON USABILITY.USABILITY_CODE = TRAWL_SPECS.USABILITY_CODE
WHERE (((TRIP_ACTIVITY.ACTIVITY_CODE)=26 Or (TRIP_ACTIVITY.ACTIVITY_CODE)=21 Or (TRIP_ACTIVITY.ACTIVITY_CODE)=27 Or (TRIP_ACTIVITY.ACTIVITY_CODE)=22) AND ((USABILITY.USABILITY_DESC)="fully usable"));`

## Updated queries 9 March 2017 in GFBioSQL
### Synoptic Surveys
includes additional season of records (~2500) from 2016-08-28 to 2016-09-26 in WCQCI

`SELECT B01_Trip.TRIP_ID, B02_Fishing_Event.FISHING_EVENT_ID, C_Activity.ACTIVITY_CODE, C_Activity.ACTIVITY_DESC, C_Gear.GEAR_DESC, B02_Fishing_Event.FE_BEGIN_DEPLOYMENT_TIME, B02_Fishing_Event.FE_END_DEPLOYMENT_TIME, B02_Fishing_Event.FE_BEGIN_RETRIEVAL_TIME, B02_Fishing_Event.FE_END_RETRIEVAL_TIME, B02_Fishing_Event.FE_START_LATTITUDE_DEGREE, B02_Fishing_Event.FE_START_LATTITUDE_MINUTE, B02_Fishing_Event.FE_END_LATTITUDE_DEGREE, B02_Fishing_Event.FE_END_LATTITUDE_MINUTE, B02_Fishing_Event.FE_START_LONGITUDE_DEGREE, B02_Fishing_Event.FE_START_LONGITUDE_MINUTE, B02_Fishing_Event.FE_END_LONGITUDE_DEGREE, B02_Fishing_Event.FE_END_LONGITUDE_MINUTE, B03_Catch.CATCH_ID, C_Species.SPECIES_CODE, C_Species.SPECIES_COMMON_NAME, C_Species.SPECIES_SCIENCE_NAME, B03_Catch.CATCH_WEIGHT, B03_Catch.CATCH_COUNT, B03_Catch.CATCH_COMMENT, C_Usability.USABILITY_DESC
FROM C_Gear INNER JOIN (C_Usability INNER JOIN ((C_Species INNER JOIN (B03_Catch INNER JOIN ((C_Activity INNER JOIN ((B01_Trip INNER JOIN B02_Fishing_Event ON B01_Trip.TRIP_ID = B02_Fishing_Event.TRIP_ID) INNER JOIN B01a_Trip_Activity ON B01_Trip.TRIP_ID = B01a_Trip_Activity.TRIP_ID) ON C_Activity.ACTIVITY_CODE = B01a_Trip_Activity.ACTIVITY_CODE) INNER JOIN B02L3_Link_Fishing_Event_Catch ON B02_Fishing_Event.FISHING_EVENT_ID = B02L3_Link_Fishing_Event_Catch.FISHING_EVENT_ID) ON B03_Catch.CATCH_ID = B02L3_Link_Fishing_Event_Catch.CATCH_ID) ON C_Species.SPECIES_CODE = B03_Catch.SPECIES_CODE) INNER JOIN B02e_Trawl_Specs ON B02_Fishing_Event.FISHING_EVENT_ID = B02e_Trawl_Specs.FISHING_EVENT_ID) ON C_Usability.USABILITY_CODE = B02e_Trawl_Specs.USABILITY_CODE) ON C_Gear.GEAR_CODE = B02_Fishing_Event.GEAR_CODE
WHERE (((C_Activity.ACTIVITY_CODE)=26 Or (C_Activity.ACTIVITY_CODE)=21 Or (C_Activity.ACTIVITY_CODE)=27 Or (C_Activity.ACTIVITY_CODE)=22) AND ((C_Usability.USABILITY_DESC)="fully usable"));`

###  Sablefish research and assessment surveys
`SELECT B01_Trip.TRIP_ID, B02_Fishing_Event.FISHING_EVENT_ID, C_Activity.ACTIVITY_CODE, C_Activity.ACTIVITY_DESC, C_Gear.GEAR_DESC, B02_Fishing_Event.FE_BEGIN_DEPLOYMENT_TIME, B02_Fishing_Event.FE_END_DEPLOYMENT_TIME, B02_Fishing_Event.FE_BEGIN_RETRIEVAL_TIME, B02_Fishing_Event.FE_END_RETRIEVAL_TIME, B02_Fishing_Event.FE_START_LATTITUDE_DEGREE, B02_Fishing_Event.FE_START_LATTITUDE_MINUTE, B02_Fishing_Event.FE_END_LATTITUDE_DEGREE, B02_Fishing_Event.FE_END_LATTITUDE_MINUTE, B02_Fishing_Event.FE_START_LONGITUDE_DEGREE, B02_Fishing_Event.FE_START_LONGITUDE_MINUTE, B02_Fishing_Event.FE_END_LONGITUDE_DEGREE, B02_Fishing_Event.FE_END_LONGITUDE_MINUTE, B03_Catch.CATCH_ID, C_Species.SPECIES_CODE, C_Species.SPECIES_COMMON_NAME, C_Species.SPECIES_SCIENCE_NAME, B03_Catch.CATCH_WEIGHT, B03_Catch.CATCH_COUNT, B03_Catch.CATCH_COMMENT, C_Usability.USABILITY_DESC
FROM C_Usability INNER JOIN ((C_Gear INNER JOIN (C_Species INNER JOIN (B03_Catch INNER JOIN ((C_Activity INNER JOIN ((B01_Trip INNER JOIN B02_Fishing_Event ON B01_Trip.TRIP_ID = B02_Fishing_Event.TRIP_ID) INNER JOIN B01a_Trip_Activity ON B01_Trip.TRIP_ID = B01a_Trip_Activity.TRIP_ID) ON C_Activity.ACTIVITY_CODE = B01a_Trip_Activity.ACTIVITY_CODE) INNER JOIN B02L3_Link_Fishing_Event_Catch ON B02_Fishing_Event.FISHING_EVENT_ID = B02L3_Link_Fishing_Event_Catch.FISHING_EVENT_ID) ON B03_Catch.CATCH_ID = B02L3_Link_Fishing_Event_Catch.CATCH_ID) ON C_Species.SPECIES_CODE = B03_Catch.SPECIES_CODE) ON C_Gear.GEAR_CODE = B02_Fishing_Event.GEAR_CODE) INNER JOIN B02d_Trap_Specs ON B02_Fishing_Event.FISHING_EVENT_ID = B02d_Trap_Specs.FISHING_EVENT_ID) ON C_Usability.USABILITY_CODE = B02d_Trap_Specs.USABILITY_CODE
WHERE (((C_Activity.ACTIVITY_CODE)=19) AND ((C_Usability.USABILITY_DESC)="fully usable"));`

###  IPHC Surveys
`SELECT B01_Trip.TRIP_ID, B02_Fishing_Event.FISHING_EVENT_ID, C_Activity.ACTIVITY_CODE, C_Activity.ACTIVITY_DESC, C_Gear.GEAR_DESC, B02_Fishing_Event.FE_BEGIN_DEPLOYMENT_TIME, B02_Fishing_Event.FE_END_DEPLOYMENT_TIME, B02_Fishing_Event.FE_BEGIN_RETRIEVAL_TIME, B02_Fishing_Event.FE_END_RETRIEVAL_TIME, B02_Fishing_Event.FE_START_LATTITUDE_DEGREE, B02_Fishing_Event.FE_START_LATTITUDE_MINUTE, B02_Fishing_Event.FE_END_LATTITUDE_DEGREE, B02_Fishing_Event.FE_END_LATTITUDE_MINUTE, B02_Fishing_Event.FE_START_LONGITUDE_DEGREE, B02_Fishing_Event.FE_START_LONGITUDE_MINUTE, B02_Fishing_Event.FE_END_LONGITUDE_DEGREE, B02_Fishing_Event.FE_END_LONGITUDE_MINUTE, B03_Catch.CATCH_ID, C_Species.SPECIES_CODE, C_Species.SPECIES_COMMON_NAME, C_Species.SPECIES_SCIENCE_NAME, B03_Catch.CATCH_WEIGHT, B03_Catch.CATCH_COUNT, B03_Catch.CATCH_COMMENT, C_Usability.USABILITY_DESC
FROM C_Usability INNER JOIN ((C_Gear INNER JOIN (C_Species INNER JOIN (B03_Catch INNER JOIN ((C_Activity INNER JOIN ((B01_Trip INNER JOIN B02_Fishing_Event ON B01_Trip.TRIP_ID = B02_Fishing_Event.TRIP_ID) INNER JOIN B01a_Trip_Activity ON B01_Trip.TRIP_ID = B01a_Trip_Activity.TRIP_ID) ON C_Activity.ACTIVITY_CODE = B01a_Trip_Activity.ACTIVITY_CODE) INNER JOIN B02L3_Link_Fishing_Event_Catch ON B02_Fishing_Event.FISHING_EVENT_ID = B02L3_Link_Fishing_Event_Catch.FISHING_EVENT_ID) ON B03_Catch.CATCH_ID = B02L3_Link_Fishing_Event_Catch.CATCH_ID) ON C_Species.SPECIES_CODE = B03_Catch.SPECIES_CODE) ON C_Gear.GEAR_CODE = B02_Fishing_Event.GEAR_CODE) INNER JOIN B02c_Longline_Specs ON B02_Fishing_Event.FISHING_EVENT_ID = B02c_Longline_Specs.FISHING_EVENT_ID) ON C_Usability.USABILITY_CODE = B02c_Longline_Specs.USABILITY_CODE
WHERE (((C_Activity.ACTIVITY_CODE)=24) AND ((C_Usability.USABILITY_DESC)="fully usable"));`

## Updated 10 April 2017 - extracted PHMA surveys from GFBioSQL
`SELECT B01_Trip.TRIP_ID, B02_Fishing_Event.FISHING_EVENT_ID, C_Activity.ACTIVITY_CODE, C_Activity.ACTIVITY_DESC, C_Gear.GEAR_DESC, B02_Fishing_Event.FE_BEGIN_DEPLOYMENT_TIME, B02_Fishing_Event.FE_END_DEPLOYMENT_TIME, B02_Fishing_Event.FE_BEGIN_RETRIEVAL_TIME, B02_Fishing_Event.FE_END_RETRIEVAL_TIME, B02_Fishing_Event.FE_START_LATTITUDE_DEGREE, B02_Fishing_Event.FE_START_LATTITUDE_MINUTE, B02_Fishing_Event.FE_END_LATTITUDE_DEGREE, B02_Fishing_Event.FE_END_LATTITUDE_MINUTE, B02_Fishing_Event.FE_START_LONGITUDE_DEGREE, B02_Fishing_Event.FE_START_LONGITUDE_MINUTE, B02_Fishing_Event.FE_END_LONGITUDE_DEGREE, B02_Fishing_Event.FE_END_LONGITUDE_MINUTE, B03_Catch.CATCH_ID, C_Species.SPECIES_CODE, C_Species.SPECIES_COMMON_NAME, C_Species.SPECIES_SCIENCE_NAME, B03_Catch.CATCH_WEIGHT, B03_Catch.CATCH_COUNT, B03_Catch.CATCH_VERIFICATION_CODE, B03_Catch.CATCH_COMMENT, C_Usability.USABILITY_DESC
FROM C_Usability INNER JOIN ((C_Gear INNER JOIN (C_Species INNER JOIN (B03_Catch INNER JOIN ((C_Activity INNER JOIN ((B01_Trip INNER JOIN B02_Fishing_Event ON B01_Trip.TRIP_ID = B02_Fishing_Event.TRIP_ID) INNER JOIN B01a_Trip_Activity ON B01_Trip.TRIP_ID = B01a_Trip_Activity.TRIP_ID) ON C_Activity.ACTIVITY_CODE = B01a_Trip_Activity.ACTIVITY_CODE) INNER JOIN B02L3_Link_Fishing_Event_Catch ON B02_Fishing_Event.FISHING_EVENT_ID = B02L3_Link_Fishing_Event_Catch.FISHING_EVENT_ID) ON B03_Catch.CATCH_ID = B02L3_Link_Fishing_Event_Catch.CATCH_ID) ON C_Species.SPECIES_CODE = B03_Catch.SPECIES_CODE) ON C_Gear.GEAR_CODE = B02_Fishing_Event.GEAR_CODE) INNER JOIN B02c_Longline_Specs ON B02_Fishing_Event.FISHING_EVENT_ID = B02c_Longline_Specs.FISHING_EVENT_ID) ON C_Usability.USABILITY_CODE = B02c_Longline_Specs.USABILITY_CODE
WHERE (((C_Activity.ACTIVITY_CODE)=28) AND ((C_Usability.USABILITY_DESC)="fully usable"));
`
