#################################################################
# Formatting of Synoptic Survey data for community analysis 
# DATA for RUBIDGE et al 2017 EBSA paper
# EBSA work -- 2017.10.13
# Katie Gale, 25 July 2016    
# 
# UPDATES:
# 29 Nov 2016:                                            
# - New data input with usability = "fully usable"               
# - Updated nrows throughout reflecting new input data           
# - Added line to specify fishing.duration.s is in SECONDS      
# - Added code to calcuate site x species matrices for SPECIES only, not for all taxa  
# - Changed rarefaction code to calculate richness on SPECIES only, not for all taxa 
# - Added Coordinates to TowxWt matrices                        
#                                                               
# 17 Feb 2017:                                          
# - added code to subset sampling points so that only one "tow" (fishing event) occurred in each planning unit     
# - removed one rex sole outlier identified by groundfish
# - reordered processing steps to account for subsampling
#
# 23 Feb 2017
# - Summed multiple weights per species per fishing_Event
#
# 27 Feb 2017
# - updated species lookup tables
# - included species that did not have weight or count but which were present verification codes 7 and 12) - this means the output pres/abs dataframe includes species PRESENT that may not have a WEIGHT or COUNT. Weight values are unaffected. 
# - added calculation and output of total weight of all taxa per tow
#
# 28 Feb 2017
# - corrected error with duplicated names/records caused by duplicates in species lookup table
# - added code to calculate number of species separately for fishes and inverts
# 
# 16 March 2017
# - added code to calculate nSp, Shannon-Weiner, Pielou's, Exponential Shannon-Wiener, and Heip's diversity and evenness indices, separately for species-level fishes and inverts
# - Note that this nSp includes only species that have abundances (i.e., not trace records)
# - Exponential Shannon-Wiener and Heip's as per Kenchington and Kenchington 2013.
# 
# 12 May 2017
# - was going to use this code to pull out all fish from synoptics for use in beta-diversity study
# - but the output matrices are for NSB only (i.e., MaPP planning grid)
# - moved relevant bits of code to other R file "Beta_Synoptic_Surveys.R"
#
# 30 June 2017
# - removed PUxWT matrix code
# - Calculating total weight of FISHES and total weight of INVERTS per tow
# - Can include records not to species
# - cleaned up code a bit
#
# 6 July
# - Removed refernence 06.30 outputs
# - added years to 02.28 matrix
# - split matrix into years and survey area (activity code)
# - create shapefiles for those splits
#
# 10 July
# - split matrix by year only not by survey area
#
# 18 Sept
# - Create shapefile of LINES  from FISHING_EVENT_ID
#################################################################

library(data.table)
library(plyr)
library(reshape)
library(vegan)
library(dplyr)
library(lubridate)
library(sp)
library(rgdal)

setwd("E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/")

#Bring in Data from the GF Bio database
newS<-data.table(read.csv("Inputs/Synoptic_22Jul16_update28Nov16.txt"))
head(newS) #97648
newS$FE_BEGIN_DEPLOYMENT_TIME<-as.character(newS$FE_BEGIN_DEPLOYMENT_TIME)
date<-as.POSIXct(newS$FE_BEGIN_DEPLOYMENT_TIME, format="%d/%m/%Y %H:%M:%S")
range(date, na.rm=T)
nrow(newS)
newS$FE_SP<-paste(newS$FISHING_EVENT_ID, newS$SPECIES_CODE,sep="-") #Add ID number to keep track of individual records
newS[newS$FE_SP %in% newS$FE_SP[duplicated(newS$FE_SP)],][1:20] #Look at duplicate species per fishing event
length(unique(newS$FE_SP)) #97555 IDs
length(unique(newS$CATCH_ID)) #97648 - so there are about 100 records of species weighed twice per tow 

unique(newS$GEAR_DESC) #all bottom trawl

#calculate Coordinates
newS$LatMid<-((newS$FE_START_LATTITUDE_DEGREE+(newS$FE_START_LATTITUDE_MINUTE/60))+(newS$FE_END_LATTITUDE_DEGREE+(newS$FE_END_LATTITUDE_MINUTE/60)))/2
newS$LonMid<-(-1)*((newS$FE_START_LONGITUDE_DEGREE+(newS$FE_START_LONGITUDE_MINUTE/60))+(newS$FE_END_LONGITUDE_DEGREE+(newS$FE_END_LONGITUDE_MINUTE/60)))/2
#plot(newS$LonMid, newS$LatMid)

#Get LINES shapefile
newS1<-newS
newS1$LatStart<-newS$FE_START_LATTITUDE_DEGREE+(newS$FE_START_LATTITUDE_MINUTE/60)
newS1$LatEnd<-newS$FE_END_LATTITUDE_DEGREE+(newS$FE_END_LATTITUDE_MINUTE/60)
newS1$LonStart<-(-1)*(newS$FE_START_LONGITUDE_DEGREE+(newS$FE_START_LONGITUDE_MINUTE/60))
newS1$LonEnd<-(-1)*(newS$FE_END_LONGITUDE_DEGREE+(newS$FE_END_LONGITUDE_MINUTE/60))
head(newS1)

linea<-as.data.frame(unique(newS1[,c("FISHING_EVENT_ID","LatStart","LatEnd","LonStart","LonEnd")]))
ranlines <- list() 
for (irow in 1:nrow(lined)) { 
  ranlines[[irow]] <- Lines(Line(rbind(as.numeric(linea[irow, c("LonStart", 
                                                               "LatStart")]), as.numeric(linea[irow, c("LonEnd", "LatEnd")]))), 
                            ID = as.character(irow)) 
} 
sldf <- SpatialLinesDataFrame(SpatialLines(ranlines), linea, match.ID = FALSE) 

proj4string(sldf)<-CRS("+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs") #set coordinate reference system
sldf <- spTransform(sldf, CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")) #project

writeOGR(sldf, dsn="E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/LineFiles", layer="SynopticFELines", driver="ESRI Shapefile")

head(line1) #4543 FEs


#create list of Fishing events and locations
FE_loc<-newS[,names(newS) %in% c("FISHING_EVENT_ID","LatMid","LonMid", "Month", "Year"), with=FALSE]
FE_loc<-FE_loc[!duplicated(FE_loc),] #4543 fishing events

#change start and end fishing dates into proper format
#To get duration, times must be in "POSIXct" format
newS$start.fishing<-as.POSIXct(newS$FE_END_DEPLOYMENT_TIME,format ="%d/%m/%Y %H:%M:%S")
newS$end.fishing<-as.POSIXct(newS$FE_BEGIN_RETRIEVAL_TIME,format ="%d/%m/%Y %H:%M:%S")

#Calculate duration of tow in seconds
newS$fishing.duration.s<-newS$end.fishing-newS$start.fishing; units(newS$fishing.duration.s)<-"secs"
str(newS$fishing.duration.s) #time in seconds
newS$fishing.duration.s<-as.numeric(newS$fishing.duration.s)
summary(newS$fishing.duration.s)

#add month and year columns
newS$month<-month(newS$start.fishing)
newS$year<-year(newS$start.fishing)

#Drop those without fishing durations (meaning either start or end fishing times were not available)
newS2<-newS[!is.na(newS$fishing.duration.s),]
nrow(newS2) #97567 species records 

#start to calculate CPUE - depending on tow and species, some records are count and some are weight
summary(newS2$CATCH_WEIGHT) #14905 NAs
summary(newS2$CATCH_COUNT) #54885 NAs

newS2$FE_SP<-paste(newS2$FISHING_EVENT_ID, newS2$SPECIES_CODE, sep="-") #Create an identifier for each species-tow record
summarybytow<-newS2[,list(total_wt=sum(CATCH_WEIGHT, na.rm=T), total_count=sum(CATCH_COUNT, na.rm=T)), by=FE_SP] #sum weights and counts for each species-tow (some species were counted more than once per tow)
head(summarybytow) #97474 species records 
length(unique(summarybytow$FE_SP)) #97474 species-tow IDs

#Add the summary to the dataframe
news3<-merge(newS2, summarybytow, by="FE_SP") #97567 
news3<-subset(news3, !duplicated(FE_SP)) #Remove duplicates (doesn't matter which one) - 97474
head(news3)
bothZero<-subset(news3, (total_wt==0 & total_count==0)) #check weights and counts that are both zero - 6891 records
head(bothZero)
unique(bothZero$CATCH_VERIFICATION_CODE) #7 = TRACE AMOUNT - VISUAL ESTIMATE, 12= CATCH NOT WEIGHED OR COUNTED, 1=SORTED AND WEIGHED, 21=TRACE WEIGHT, WITH COUNT , 2=SUB-SAMPLED AND WEIGHED
seven<-bothZero[bothZero$CATCH_VERIFICATION_CODE==7,] #6780 that were not counted or weighed (Trace amount)- fine.
twelve<-bothZero[bothZero$CATCH_VERIFICATION_CODE==12,] #74 that were not counted or weighed - fine.

one<-bothZero[bothZero$CATCH_VERIFICATION_CODE==1,] #26 that were apparently sorted and weighed but have no weight or count
twentyone<-bothZero[bothZero$CATCH_VERIFICATION_CODE==21,] #4 that were apparently counted but have no weight or count
two<-bothZero[bothZero$CATCH_VERIFICATION_CODE==2,] #7 that were apparently weighed but have no weight or count
#Will drop these 37 records.

news4<-subset(news3, !(news3$FE_SP %in% c(one$FE_SP, twentyone$FE_SP, two$FE_SP))) 
nrow(news4)-nrow(news3) #37 removed = 97437

#Calculate CPUE Weight per Hour (kg/hr)
news4$WTperHR<-news4$total_wt/(news4$fishing.duration.s/3600)
news4$WTperHR[news4$WTperHR==Inf]<-0

#Calculate CPUE Count per Hour (#/hr)
news4$CTperHR<-news4$total_count/(news4$fishing.duration.s/3600)
news4$CTperHR[news4$CTperHR==Inf]<-0

#Remove fishing trips that were less than 3 minutes long - causes outliers in the extrapolated CPUE
news5<-subset(news4, fishing.duration.s>180) #97437 records
nrow(news5)
length(unique(news5$FE_SP)) #97437

#Remove known outliers
plot(news5$WTperHR[news5$SPECIES_SCIENCE_NAME=="GLYPTOCEPHALUS ZACHIRUS"]) #one identified outlier
news5$FISHING_EVENT_ID[news5$SPECIES_SCIENCE_NAME=="GLYPTOCEPHALUS ZACHIRUS"&news5$WTperHR>2000]
news5[news5$FISHING_EVENT_ID=="2535929",]
news5$USABILITY_DESC<-as.character(news5$USABILITY_DESC)
news5$USABILITY_DESC[news5$FE_SP=="2535929-610"]<-"EXPERT DETERMINED CATCH LIKELY TOO HIGH"
news5$USABILITY_DESC<-as.factor(news5$USABILITY_DESC)
news5<-news5[news5$USABILITY_DESC!="EXPERT DETERMINED CATCH LIKELY TOO HIGH",] #97436

#Subset columns to be more manageable
synDat<-news5[,names(news5) %in% c("FE_SP","TRIP_ID","ACTIVITY_CODE","TRIP_START_DATE","year", "month","FISHING_EVENT_ID",
                                   "GEAR_DESC","LatMid","LonMid","start.fishing","end.fishing","fishing.duration.s",
                                   "SPECIES_CODE","SPECIES_SCIENCE_NAME","total_wt","total_count","WTperHR","CTperHR"    ), with=F]

#Check records that don't have science names
synDat$SPECIES_CODE[synDat$SPECIES_SCIENCE_NAME==""] #these M codes are egg cases & can be dropped
synDat<-synDat[SPECIES_SCIENCE_NAME!="",] #Drop blank names; 97343 records 

##Fix names
#Bring in SpeciesNames from WoRMS ##should really do this before merging/dropping data
specNames<-read.delim("Inputs/Taxonomy_2017.02.27.txt", sep="\t")
specNames<-specNames[,names(specNames) %in% c("ScientificName","ScientificName_accepted","Phylum","Class","Order")]
names(specNames)[1:2]<-c("OriginalName","ValidName")
specNames<-unique(specNames)

#Make both original and WoRMS lookup names lowercase to match
specNames$OriginalName<-tolower(specNames$OriginalName)
synDat$OriginalName<-tolower(synDat$SPECIES_SCIENCE_NAME)
unique(synDat$OriginalName)

#join corrected names
nrow(synDat) #97343
length(unique(synDat$FE_SP)) #97343
synDat2<-merge(as.data.frame(synDat), specNames, by="OriginalName", all.x=T, all.y=F)

summary(synDat2$ValidName)
#Look at NAs and blanks
unique(synDat2$OriginalName[is.na(synDat2$ValidName)]) #Acceptable to leave these as NA - remove them
unique(synDat2$OriginalName[synDat2$ValidName==""])  #These don't have valid names, so remove them

#remove NA records
synDat3<-data.table(synDat2[!is.na(synDat2$ValidName),]) #97312
synDat3<-data.table(synDat3[synDat3$ValidName!="",]) #97298
nrow(synDat3) #97298
length(unique(synDat3$FE_SP)) #97298
synDat3[synDat3$FE_SP %in% synDat3$FE_SP[duplicated(synDat3$FE_SP)],]

synDat3[synDat3$ValidName==""] #check that that none of the names are blank

synDat3<-synDat3[,!(names(synDat3) %in% c("OriginalName","SPECIES_SCIENCE_NAME")),with=FALSE] #Remove these 2 columns

#Bring in file that has taxon level for each name
taxonLevel<-read.csv("Inputs/Species_TaxonLevel_2017.02.27.csv")
names(taxonLevel)<-c("ValidName","TaxonLevel")
taxonLevel<-unique(taxonLevel)

#Add taxonlevel to the dataframe
synDat4<-merge(synDat3,taxonLevel, by="ValidName", all.x=T, all.y=F )
nrow(synDat4) #97298
synDat4$TaxonLevel<-tolower(synDat4$TaxonLevel)

#Order columns
setcolorder(synDat4, c("FE_SP","TRIP_ID","ACTIVITY_CODE","TRIP_START_DATE","year", "month","FISHING_EVENT_ID",
                       "GEAR_DESC","LatMid","LonMid","start.fishing","end.fishing","fishing.duration.s",
                       "SPECIES_CODE","ValidName","Phylum","Class","Order","TaxonLevel","total_wt","total_count","WTperHR","CTperHR"))

subset(synDat4, is.na(TaxonLevel)) #check for no level - good

#Create object with only species-level records
synDatSpecies<-subset(synDat4, TaxonLevel=="species") #88000 are to species only

length(unique(synDat4$ValidName[(synDat4$FE_SP %in% synDatSpecies$FE_SP)])) #474 species
# 
# write.csv(synDat4, "../2017.02.27 Subsample/Outputs/GFSynoptic_AllTaxa_97298_2017.02.28.csv",row.names=F)
# write.csv(synDatSpecies, "../2017.02.27 Subsample/Outputs/GFSynoptic_Species_88000_2017.02.28.csv",row.names=F)
# synDat4<-read.csv("E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/Textfiles/Synoptics/GFSynoptic_AllTaxa_97298_2017.02.28.csv")

##########
#Grid data
########
library(maptools)
library(rgdal)

#Start with the dataframe of species records
synDat5<-data.table(synDat4)
head(synDat5)

#Add duplicate columns for Lat and Long - helps later
synDat5$LatMid1<-synDat5$LatMid
synDat5$LonMid1<-synDat5$LonMid

#Turn this dataframe into a spatial object
coordinates(synDat5)<-~LonMid+LatMid #tell R what the coordates are
proj4string(synDat5)<-CRS("+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs") #set coordinate reference system
DatShp <- spTransform(synDat5, CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")) #project

#import 1 km planning grid and set projection
PUgrid<-readShapeSpatial("Inputs/MaPP_PU_Final/MaPP_PU_Final.shp")
proj4string(PUgrid)<-CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs") #project

####
# spatial join of trawl points with 1km grid - overlay could take a long time
#####
overlay<-over(DatShp,PUgrid) #Spatial join our dataset with the grids
DatwGrid<-cbind(DatShp@data,overlay ) #Bind the species data with the overlay info 
points(DatwGrid$LonMid[is.na(DatwGrid$PU_ID)], DatwGrid$LatMid[is.na(DatwGrid$PU_ID)], col="red") #The points in WCVI don't have grid cells (outside planning area)

DatwGrid<-DatwGrid[,!(names(DatwGrid) %in% c("OBJECTID_1","Shape_Le_1","Shape_Area")) ] #drop unwanted columns from grid shapefile
head(DatwGrid)

#Create a shapefile that has just the PU grids that contain data
#if gridID=NA, then that point was outside of our study area
populated<-(unique(DatwGrid$PU_ID)) #get list of populated grid cells
length(unique(PUgrid$PU_ID)) #113839 grid cells initiallly 
gridSubAllTaxa<-subset(PUgrid,PUgrid$PU_ID %in% populated) #select only those grid cells that our data populates
length(unique(gridSubAllTaxa$PU_ID)) #   3279 grid cells are populated by all taxa 

#Write this shapefile for later use
# writeSpatialShape(gridSubAllTaxa,"../2017.02.27 Subsample/Outputs/Shapefiles/PU_with_Data_2017.02.27.shp") 

DatwGrid2<-DatwGrid[!is.na(DatwGrid$PU_ID),] #Drop records that are outside the planning region (75640 records remain)
head(DatwGrid2)

#Look at planning units that have multiple surveys in them
#Select planning units that have more than 1 fishing event

PU_FE<-cbind.data.frame(DatwGrid2$FISHING_EVENT_ID,DatwGrid2$PU_ID)
PU_FE<-unique(PU_FE)
names(PU_FE)<-c("FISHING_EVENT_ID","PU_ID")
head(PU_FE) #all fishing events with associated planning unit


PUCount<-ddply(PU_FE,  c("PU_ID"), summarize,nFEinPU=length(unique(FISHING_EVENT_ID)))
head(PUCount)

#Output - number of fishing evnets per planning unit
PUGridCount<-merge(PUgrid,PUCount,  by="PU_ID")
head(PUGridCount@data)
PUGridCount@data$nFEinPU[is.na(PUGridCount@data$nFEinPU)]<-0
# writeSpatialShape(PUGridCount, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Shapefiles/NFEperPU.shp")

dupPU<-subset(PUCount, nFEinPU>1) #select planning units that have more than one fishing event in them
dupPU  #309 planning units have more than one fishing event

#randomly select one FISHING_EVENT from each duplicated PU

### !!!
# ##NOTE EVERYTIME THIS IS RUN, A DIFFERENT RESULT IS OBTAINED WHICH CHANGES THE N SPECIES
# ### !!!
# subs<-PU_FE %>% group_by(PU_ID) %>% sample_n(size = 1) #Select one Fishing event per planning unit
# head(subs)
# subs$FISHING_EVENT_ID
# 
# DatwGrid2Subset<-DatwGrid2[DatwGrid2$FISHING_EVENT %in% subs$FISHING_EVENT_ID,] #subset the dataset to include only those fishing events selected above
# head(DatwGrid2Subset )
# nrow(DatwGrid2Subset ) #68811
# # write.csv(DatwGrid2Subset, "..//2017.02.27 Subsample/Outputs/TextfilesUPDATED/GFSynoptic_Subsetted_AlltaxaInPU_68811_2017.02.28x.csv",row.names=F)

###
# START HERE AFTER SUBSETTING TO AVOID OVERWRITING RANDOMIZATION

#################################
#Get Site x Species Matrices
################################
# Start here 

DatwGrid2Subset<-read.csv("E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/TextFiles/Synoptics/GFSynoptic_Subsampled_AlltaxaInPU_68811_2017.02.28.csv")
head(DatwGrid2Subset)
DatwGrid2Subset$start.fishing<-as.POSIXct(DatwGrid2Subset$start.fishing)
range(DatwGrid2Subset$start.fishing)
table(DatwGrid2Subset$month)/688.11
nrow(DatwGrid2Subset) #68811

################################
#TOW X SPECIES Matrices
################################
FELocDatDate<-unique(cbind.data.frame(DatwGrid2Subset$FISHING_EVENT_ID, DatwGrid2Subset$LatMid1, DatwGrid2Subset$LonMid1, DatwGrid2Subset$year, DatwGrid2Subset$month,DatwGrid2Subset$ACTIVITY_CODE) ) #fishing event locations
names(FELocDatDate)<-c("FISHING_EVENT_ID","LatMid","LonMid","Year","Month","ACTIVITY_CODE")
head(FELocDatDate)

speciesLevelNames<-unique(DatwGrid2Subset$ValidName[DatwGrid2Subset$TaxonLevel=="species"])

# #Tow by taxa weight (all taxon levels)
perSetWt1<-DatwGrid2Subset[,c("FISHING_EVENT_ID","ValidName","WTperHR")]
perSetWtSUBMelt<-melt(perSetWt1, id.vars=c("FISHING_EVENT_ID","ValidName"))
perSetWtSUBCast<-cast(perSetWtSUBMelt, FISHING_EVENT_ID~ValidName, fun=sum) #site x species
dim(perSetWtSUBCast) #3279 x 718 taxa +1 col
perSetWtSUBCast2<-join(perSetWtSUBCast, FELocDatDate, by="FISHING_EVENT_ID") #attach locations
dim(perSetWtSUBCast2) #3279 x 718 taxa +6 col
head(perSetWtSUBCast2)
write.csv(perSetWtSUBCast2, "../2017.02.27 Subsample/Outputs/Textfiles/Synoptics/GFSynoptic_Subsetted_TowxWt_AlltaxaInPU_2017.02.28_date.csv",row.names=F)

# #Tow by species weight (species level only)
perSetWtSpSUBCast2<-perSetWtSUBCast2[,names(perSetWtSUBCast2) %in% c(paste(speciesLevelNames),"LatMid","LonMid","FISHING_EVENT_ID", "Year","Month","ACTIVITY_CODE")]
head(perSetWtSpSUBCast2)
dim(perSetWtSpSUBCast2) #3279 x 444 taxa + 6 col
# # write.csv(perSetWtSpSUBCast2, "../2017.02.27 Subsample/Outputs//TextfilesUPDATED/GFSynoptic_Subsetted_TowxWt_SpeciesInPU_2017.02.28.csv",row.names=F)

# # #Tow by taxa presence (all taxon levels) #Not sufficient to change the tow-wt matrix to P/A, since there may be some species that weren't weight but are still present
# perSetP1<-DatwGrid2Subset[,c("FISHING_EVENT_ID","ValidName","FE_SP")]
# perSetPSUBMelt<-melt(perSetP1, id.vars=c("FISHING_EVENT_ID","ValidName"))
# perSetPSUBCast<-cast(perSetPSUBMelt, FISHING_EVENT_ID~ValidName, fun=length) #site x species
# dim(perSetPSUBCast) #3279 x 718 taxa +1 col
# perSetPSUBCast2<-join(perSetPSUBCast, FELocDatDate, by="FISHING_EVENT_ID") #attach locations
# dim(perSetPSUBCast2) #3279 x 718 taxa +3 col
# head(perSetPSUBCast2)
# nTowperSp<-colSums(perSetPSUBCast2[2:(ncol(perSetPSUBCast2)-2)]) #check that each species is present at least once
# summary(nTowperSp) #at least one record per taxon
# # write.csv(perSetPSUBCast2, "../2017.02.27 Subsample/Outputs//TextfilesUPDATED/GFSynoptic_Subsetted_TowxPres_AlltaxaInPU_2017.02.28.csv",row.names=F)
# 
# #Tow by species presence (species level only)
perSetPSpSUBCast2<-perSetPSUBCast2[,names(perSetPSUBCast2) %in% c(paste(speciesLevelNames),"LatMid","LonMid","FISHING_EVENT_ID", "Year","Month","ACTIVITY_CODE")]
head(perSetPSpSUBCast2)
dim(perSetPSpSUBCast2) #3279 x 444 taxa + 6 col
# nTowperSp<-colSums(perSetPSpSUBCast2[2:(ncol(perSetPSpSUBCast2)-2)])
# summary(nTowperSp) #at least one record per species
# nSpperTow<-cbind.data.frame(perSetPSpSUBCast2$FISHING_EVENT_ID,rowSums(perSetPSpSUBCast2[2:(ncol(perSetPSpSUBCast2)-2)]),perSetPSpSUBCast2$LatMid,perSetPSpSUBCast2$LonMid)
# names(nSpperTow)<-c("FISHING_EVENT_ID","nSp","LatMid","LonMid")
# head(nSpperTow)
# # write.csv(perSetPSpSUBCast2, "../2017.02.27 Subsample/Outputs/TextfilesUPDATED/GFSynoptic_Subsetted_TowxPres_SpeciesInPU_2017.02.28.csv",row.names=F)
# # write.csv(nSpperTow, "../2017.02.27 Subsample/Outputs/TextfilesUPDATED/GFSynoptic_Subsetted_nSpperTow_2017.02.28.csv",row.names=F)

#Separate fish and invertebrates for richness (remember to get SPECIES-level taxa only!) #Don't need to re-calculate matrices, should be able to just grab columns of the names we want
#GET INVERT NAMES
inverts<-DatwGrid2Subset[DatwGrid2Subset$Class %in% c("Ascidiacea","Thaliacea")|DatwGrid2Subset$Phylum %in% c("Porifera","Echinodermata","Arthropoda","Cnidaria","Mollusca",   "Annelida","Brachiopoda"    ,
                                                      "Bryozoa","Ctenophora","Nemertea","Platyhelminthes", "Sipuncula")|DatwGrid2Subset$ValidName=="Tunicata",]
length(unique(inverts$ValidName[inverts$TaxonLevel=="species"])) #221 species-level taxa
invertNames<-unique(inverts$ValidName)
invertSpNames<-unique(inverts$ValidName[inverts$TaxonLevel=="species"])

#FISHES
fishes<-DatwGrid2Subset[DatwGrid2Subset$Class %in% c("Actinopteri","Elasmobranchii","Myxini","Holocephali","Petromyzonti")|DatwGrid2Subset$ValidName %in% c("Gnathostomata","Pisces"),]
length(unique(fishes$ValidName[fishes$TaxonLevel=="species"])) #222
fishNames<-unique(fishes$ValidName)
fishSpNames<-unique(fishes$ValidName[fishes$TaxonLevel=="species"])

#Other Taxa
mammals<-DatwGrid2Subset[DatwGrid2Subset$Class %in% c("Mammalia"),]
animalia<-DatwGrid2Subset[DatwGrid2Subset$Phylum=="null",]
counted<-unique(c(inverts$FE_SP, fishes$FE_SP, mammals$FE_SP, animalia$FE_SP ))
length(counted) #68811 - all taxa accounted for

nrow(inverts) #18597
nrow(fishes) #50161

#Subset site x species matrices with ABUNDANCE to only include species-level taxa for inverts and fishes
#NOTE these N species counts and diversity matrices inlcude ONLY species that were weighed. TRACE ANIMALS NOT INCLUDED.
InvertMatrix<-perSetWtSUBCast2[,names(perSetWtSUBCast2) %in% c(paste(invertSpNames),"LatMid","LonMid","FISHING_EVENT_ID")]
head(InvertMatrix)
dim(InvertMatrix) #3279 sites x 221 sp + 3cols
InvertMatrixSpec<-InvertMatrix[2:(ncol(InvertMatrix)-2)] #get species data only from here (drop FE and lat long)

#INVERTS
summaryInvert<-cbind.data.frame(FISHING_EVENT_ID=InvertMatrix$FISHING_EVENT_ID,
                              nSp=specnumber(InvertMatrixSpec),
                              Shan=diversity(InvertMatrixSpec,index="shannon"),
                              J_Pielou=(diversity(InvertMatrixSpec,index="shannon")/log(specnumber(InvertMatrixSpec))),
                              ExpShan=exp(diversity(InvertMatrixSpec,index="shannon")),
                              E_Heip=((exp(diversity(InvertMatrixSpec,index="shannon"))-1)/(specnumber(InvertMatrixSpec)-1)),                              
                              LatMid=InvertMatrix$LatMid,LonMid=InvertMatrix$LonMid)
head(summaryInvert)
summaryInvert[1:50,]
range(summaryInvert$nSp) #0-12
write.csv(summaryInvert, "../2017.02.27 Subsample/Outputs/Textfiles/GFSynoptic_Subsampled_diversitySummaryInvert_2017.03.16.csv",row.names=F)

#Total Weight for all inverts (all taxa)
InvertMatrixAllTaxa<-perSetWtSUBCast2[,names(perSetWtSUBCast2) %in% c(paste(invertNames),"LatMid","LonMid","Year","Month","ACTIVITY_CODE","FISHING_EVENT_ID")]
InvertMatrixAllTaxaTaxa<-InvertMatrixAllTaxa[2:(ncol(InvertMatrixAllTaxa)-5)] #get taxa data only from here (drop FE and lat long and dates)
head(InvertMatrixAllTaxaTaxa)
InvertBiomass<-cbind.data.frame(FISHING_EVENT_ID=InvertMatrixAllTaxa$FISHING_EVENT_ID,WtperHr_Total=rowSums(InvertMatrixAllTaxaTaxa),
                                LatMid=InvertMatrixAllTaxa$LatMid,LonMid=InvertMatrixAllTaxa$LonMid)
write.csv(InvertBiomass, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/Synoptics/GFSynoptic_Subsampled_toalBiomassInvert_2017.06.30.csv")

#FISHES - use tow x species matrix that has date and survey type attached
FishMatrix<-perSetWtSpSUBCast2_Date[,names(perSetWtSpSUBCast2_Date) %in% c(paste(fishSpNames),"LatMid","LonMid","FISHING_EVENT_ID","Year","Month","ACTIVITY_CODE")]
head(FishMatrix)
dim(FishMatrix) #3279 sites x 222 sp + 6 cols

plot(FishMatrix$LonMid, FishMatrix$LatMid, col=FishMatrix$ACTIVITY_CODE, xlim=c(-134,-120),ylim=c(45,60))
legend("bottomleft",legend=c("Queen Char. Sound","W. Coast Van. Is","Hecate Strait","W. Coast Haida Gwaii"), pch=16,col=unique(FishMatrix$ACTIVITY_CODE))

FishMatrixSpec<-FishMatrix[2:(ncol(FishMatrix)-2)] #get species data only from here (drop FE and lat long)
head(FishMatrixSpec)

summaryFish<-cbind.data.frame(FISHING_EVENT_ID=FishMatrix$FISHING_EVENT_ID,
                              nSp=specnumber(FishMatrixSpec),
                                Shan=diversity(FishMatrixSpec,index="shannon"),
                                  J_Pielou=(diversity(FishMatrixSpec,index="shannon")/log(specnumber(FishMatrixSpec))),
                                   ExpShan=exp(diversity(FishMatrixSpec,index="shannon")),
                                    E_Heip=((exp(diversity(FishMatrixSpec,index="shannon"))-1)/(specnumber(FishMatrixSpec)-1)),                              
                                      LatMid=FishMatrix$LatMid,LonMid=FishMatrix$LonMid)

head(summaryFish)
range(summaryFish$nSp) #1-32

write.csv(summaryFish, "../2017.02.27 Subsample/Outputs/Textfiles/GFSynoptic_Subsampled_diversitySummaryFish_2017.03.16.csv",row.names=F)

#Total Weight for all fish (all taxa)
FishMatrixAllTaxa<-perSetWtSUBCast2[,names(perSetWtSUBCast2) %in% c(paste(fishNames),"LatMid","LonMid","Year","Month","ACTIVITY_CODE","FISHING_EVENT_ID")]
FishMatrixAllTaxaTaxa<-FishMatrixAllTaxa[2:(ncol(FishMatrixAllTaxa)-5)] #get taxa data only from here (drop FE and lat long and dates)
head(FishMatrixAllTaxaTaxa)
FishBiomass<-cbind.data.frame(FISHING_EVENT_ID=FishMatrixAllTaxa$FISHING_EVENT_ID,WtperHr_Total=rowSums(FishMatrixAllTaxaTaxa),
                                LatMid=FishMatrixAllTaxa$LatMid,LonMid=FishMatrixAllTaxa$LonMid)

write.csv(FishBiomass, "E:/Documents/Projects/NearshoreEBSA/GFData_Hotspots/2017.02.27 Subsample/Outputs/Textfiles/Synoptics/GFSynoptic_Subsampled_toalBiomassFish_2017.06.30.csv")

#Total weight per hour of all taxa per tow
perSetWtSUBCast2$totalWtperHr<-rowSums(perSetWtSUBCast2[,c(2:(ncol(perSetWtSUBCast2)-2))])
head(perSetWtSUBCast2)[,c(1:5,ncol(perSetWtSUBCast2))]

totalWtPerHrPerTow<-perSetWtSUBCast2[,c(1,(ncol(perSetWtSUBCast2)-2):ncol(perSetWtSUBCast2))]
head(totalWtPerHrPerTow)
write.csv(totalWtPerHrPerTow,"../2017.02.27 Subsample/Outputs/GFSynoptic_Subsetted_TotalWtPerHr_AllTaxa_2017.02.28.csv",row.names=F)

#Separate TowXWt Matrix by year and make shapefiles and text files
dat<-unique(data.frame(year=perSetWtSUBCast2$Year)) #, surv=perSetWtSUBCast2$ACTIVITY_CODE))
for (i in 1:nrow(dat)){
  sub<-perSetWtSUBCast2[perSetWtSUBCast2$Year==dat[i,1],] #&perSetWtSUBCast2$ACTIVITY_CODE==dat[i,2],]
  # write.csv(sub,paste("../2017.02.27 Subsample/Outputs/Textfiles/Synoptics/TowxWtByYear/Syn_Sub_TowxWt_", dat[i,2],"_",dat[i,1],".csv", sep=""), row.names=F)
  sub$lat<-sub$LatMid
  sub$lon<-sub$LonMid
  coordinates(sub)<-~lon+lat
  proj4string(sub)<-CRS("+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs") #set coordinate reference system
  subshp <- spTransform(sub, CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")) #project
  writeOGR(subshp, "../2017.02.27 Subsample/Outputs/Shapefiles/Synoptics/TowxWtByYear",layer=paste("/Syn_Sub_TowxWt_",
                                                                                                   #dat[i,2],"_",
                                                                                                   dat[i,1], sep=""),driver="ESRI Shapefile" )
  db<-read.dbf(paste("../2017.02.27 Subsample/Outputs/Shapefiles/Synoptics/TowxWtByYear/Syn_Sub_TowxWt_",
                     #dat[i,2],"_",
                     dat[i,1],".dbf",sep=""))
  head(db)
  names(db)<-names(sub)
  head(db)
  write.dbf(db,paste("../2017.02.27 Subsample/Outputs/Shapefiles/Synoptics/TowxWtByYear/Syn_Sub_TowxWt_",
                     #dat[i,2],"_",
                     dat[i,1],".dbf",sep="") )
  }


#Tow length by NSpecies for species in final dataset
library(plyr)
DatwGrid2Subset<-read.csv("E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/TextFiles/Synoptics/GFSynoptic_Subsampled_AlltaxaInPU_68811_2017.02.28.csv")
head(DatwGrid2Subset)
DatwGrid2SubsetSp<-subset(DatwGrid2Subset, TaxonLevel=="species")
str(DatwGrid2SubsetSp$fishing.duration.s)
look<-ddply(DatwGrid2SubsetSp, c("FISHING_EVENT_ID"), summarize, towDur=mean(fishing.duration.s),nSp=length(unique(SPECIES_CODE)))
head(look)
look<-look[order(look$towDur),]
png("E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/SynopticTowbyRichness.png", res=250, height=9, width=12, units="cm")
par(mar=c(3,3,1,1))
plot(look$towDur/60, look$nSp,  pch=21, bg="lightblue", cex=0.6, ylab="", xlab="")
title(xlab="Tow duration (min)", ylab="Number of species recorded", mgp=c(2,2,0))
dev.off()

quantile(look$towDur/60, seq(0.05,1, 0.05))
