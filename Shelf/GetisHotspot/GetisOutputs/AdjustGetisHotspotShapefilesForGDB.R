## Recode Getis-ord sampling points into Hotspot/Not Hotspot 

setwd("D:/Documents/Projects/EBSAs/Analyses/Shelf/GetisHotspot/GetisOutputs/1.IntermediateShapefiles/")

library(foreign)
library(rgdal)

to.read<-list.files(pattern="*_.dbf$")
to.read<-to.read[!to.read %in% c("Synoptic_Hotspot_Invert_Richness_.dbf","Synoptic_Hotspot_Fish_Richness_.dbf","PHMA_Hotspot_Fish_Richness_.dbf","PHMA_Hotspot_Fish_Biomass_.dbf")]

# for (i in 1:length(to.read)){
#   file<-foreign::read.dbf(to.read[i])
#   head(file)
#   file<-file[,!names(file)%in% c("test")]
#   file$Hotspot<-ifelse(file$Gi_Bin>0,"1","0")
# 
# foreign::write.dbf(file, to.read[i])
#   }


#Combine Synoptics into One File
synopticFootprint<-readOGR("Synoptic_Hotspot_Fish_Biomass_.shp")
head(synopticFootprint@data)
synopticFootprint$HotFishBiom<-ifelse(synopticFootprint$Gi_Bin>0,"1","0")

fishShan<-read.dbf("Synoptic_Hotspot_Fish_Shannon_.dbf")
synopticFootprint$HotFishDiv<-ifelse(fishShan$Gi_Bin>0,"1","0")

InvBiom<-read.dbf("Synoptic_Hotspot_Invert_Biomass_.dbf")
synopticFootprint$HotInvBiom<-ifelse(InvBiom$Gi_Bin>0,"1","0")

InvShan<-read.dbf("Synoptic_Hotspot_Invert_Shannon_.dbf")
synopticFootprint$HotInvDiv<-ifelse(InvShan$Gi_Bin>0,"1","0")

synopticFootprint@data<-synopticFootprint@data[,!names(synopticFootprint@data) %in% c("WtperHr_To","GiZScore","GiPValue", "Gi_Bin","Hotspot")]

head(synopticFootprint@data)
writeOGR(synopticFootprint,"../2.FinalHotspotPolygons","SynopticHotspotFootprint", driver="ESRI Shapefile", overwrite=T)
names<-read.dbf("../2.FinalHotspotPolygons/SynopticHotspotFootprint.dbf")
names(names)<-names(synopticFootprint)
write.dbf(names, "../2.FinalHotspotPolygons/SynopticHotspotFootprint.dbf")

#PHMA
phmaFootprint<-readOGR("PHMA_Hotspot_Fish_Shannon_.shp")
head(phmaFootprint)
phmaFootprint$HotFishDiv<-ifelse(phmaFootprint$Gi_Bin>0,"1","0")
phmaFootprint<-phmaFootprint[,names(phmaFootprint)%in% c("SOURCE_ID","HotFishDiv")]

writeOGR(phmaFootprint,"../2.FinalHotspotPolygons","PHMAHotspotFootprint", driver="ESRI Shapefile", overwrite=T)
names<-read.dbf("../2.FinalHotspotPolygons/PHMAHotspotFootprint.dbf")
names(names)<-names(phmaFootprint)
write.dbf(names, "../2.FinalHotspotPolygons/PHMAHotspotFootprint.dbf")